import json

from config.config import Config

employees = '{"employee_name": "Test Employee","employee_salary": "20000", "employee_age": "23", "profile_image": ""}'
# file_name = "employee.json"
dict_employee = json.load(employees)


def parse_json_file(file_name):
    with open(Config.JSON_PATH + file_name) as employee:
        employee_data = json.load(employee)
