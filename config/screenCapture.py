import allure
from allure_commons.types import AttachmentType


def capture_screenshot(context, name):
    allure.attach(context.driver.get_screenshot_as_png(), name=name, attachment_type=AttachmentType.PNG)