import os
import pathlib


class Config:
    home = pathlib.Path(__file__).parents[1]
    CHROME_DRIVER_PATH = str(pathlib.Path(__file__).parent) + "\\drivers\\chromedriver.exe"
    BASE_URL = "https://avm.pinpointcomms.com.au/WebAvm4"
    BROWSER_TYPE = "chrome"
    ENV = "uat"
    EXCEL_PATH = os.path.join(home) + "\\data\\data.xlsx"
    JSON_PATH = os.path.join(home)+"\\payloads\\"
    API_URL = "http://dummy.restapiexample.com/api/v1"

    ENVIRONMENTS = [{
        'environment': "aus1",
        'url': 'https://avm.pinpointcomms.com.au/WebAvm4'
    }, {
        'environment': "aus2",
        'url': 'https://avm.pinpointcomms.com.au/WebAvm4'
    }, {
        'environment': "aus3",
        'url': 'https://cloud.pinpointcomms.com.au/WebAvm4/Authentication/Account/Login?ReturnUrl=%2fwebavm4%2fmain'
    }, {
        'environment': "ivms",
        'url': 'https://avm.pinpointcomms.com.au/WebAvm4'
    }, {
        'environment': "unitywater",
        'url': 'https://avm.pinpointcomms.com.au/webavm4UnityWater'
    }, {
        'environment': "tnt",
        'url': 'https://avm.pinpointcomms.com.au/WebAvm4TNT'
    }, {
        'environment': "ausgrid",
        'url': 'https://uat.pinpointcomms.com.au/WebAvm4/Authentication/'
    }, {
        'environment': "uat",
        'url': 'https://uat.pinpointcomms.com.au/WebAvm4/Authentication/'
    }]

    DRIVERS = [{
        'browser': 'chrome',
        'path': os.path.join(home) + "\\drivers\\chromedriver.exe"
    }, {
        'browser': 'firefox',
        'path': os.path.join(home) + "\\drivers\\geckodriver.exe"
    }, {
        'browser': 'ie',
        'path': os.path.join(home) + "\\drivers\\IEDriverServer.exe"
    }, {
        'browser': 'safari',
        'path': "/usr/bin/safaridriver"
    }]

    @staticmethod
    def get_environment(env):
        if not Config.ENV:
            return Config.ENVIRONMENTS[7].get("url")
        elif env.lower() == 'aus1':
            return Config.ENVIRONMENTS[0].get("url")
        elif env.lower() == 'aus2':
            return Config.ENVIRONMENTS[1].get("url")
        elif env.lower() == 'aus3':
            return Config.ENVIRONMENTS[2].get("url")
        elif env.lower() == 'ivms':
            return Config.ENVIRONMENTS[3].get("url")
        elif env.lower() == 'unitywater':
            return Config.ENVIRONMENTS[4].get("url")
        elif env.lower() == 'tnt':
            return Config.ENVIRONMENTS[5].get("url")
        elif env.lower() == 'uat':
            return Config.ENVIRONMENTS[7].get("url")
        else:
            raise Exception("Environment type {} is not defined".format(Config.ENV))
