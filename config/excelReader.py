import openpyxl
from config.config import Config
import logging
import pdb

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


def get_excel_data(environment):
    dictionary = {}
    book = openpyxl.load_workbook(Config.EXCEL_PATH)
    sheet = book.active
    logging.info(sheet)
    for i in range(1, sheet.max_row + 1):
        logging.info(sheet.cell(row=i, column=1).value)
        if sheet.cell(row=i, column=1).value.lower() == environment:
            for j in range(2, sheet.max_column + 1):
                dictionary[sheet.cell(row=1, column=j).value.lower()] = sheet.cell(row=i, column=j).value
    logging.info(dictionary)
    return dictionary
