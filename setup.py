from setuptools import setup
from config.config import Config

setup(name='NetStar Automation Framework',
      version='1.0',
      description='This includes all the NetStar automation project code',
      author='Eranda Kodagoda',
      author_email='eranda.k@eyepax.com',
      url=Config.BASE_URL,
      packages=[
          'pages',
          'config',
          'features.steps',
      ],
      )
