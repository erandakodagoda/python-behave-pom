import subprocess
import argparse
from datetime import datetime
import os
import pathlib
from shutil import rmtree
from glob import glob


def get_unique_run_id():
    if os.environ.get("BUILD_NUMBER"):
        unique_run_id = os.environ.get("BUILD_NUMBER")
    elif os.environ.get("CUSTOM_BUILD_NUMBER"):
        unique_run_id = os.environ.get("CUSTOM_BUILD_NUMBER")
    else:
        unique_run_id = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

    os.environ['UNIQUE_RUN_ID'] = unique_run_id

    return unique_run_id


def create_output_directory(prefix='results_'):
    global run_id
    if not run_id:
        raise Exception("Variable 'run_id' is not set. Unable to create output directory")

    curr_file_path = pathlib.Path(__file__).parent.absolute()
    dir_to_create = os.path.join(curr_file_path, prefix + str(run_id))
    os.mkdir(dir_to_create)

    print(f"Created output directory: {dir_to_create}")

    return dir_to_create


def clean_repo():
    path = os.getcwd()
    pattern = os.path.join(path, "results_*")

    for item in glob(pattern):
        if not os.path.isdir(item):
            continue
        rmtree(item)


if __name__ == '__main__':
    run_id = get_unique_run_id()

    parser = argparse.ArgumentParser()
    parser.add_argument('--test_dir', required=True, help="Feature File Location")
    parser.add_argument('--behave_options', type=str, required=False,
                        help="String of behave options. For Example tags like '-t <tag name>'")
    parser.add_argument('--run_allure', choices=['true', 'True', 'false', 'False'], required=False,
                        help="To run allure report server at the end of the test run or not.")
    args = parser.parse_args()
    test_dir = args.test_dir
    run_allure = args.run_allure
    behave_options = '' if not args.behave_options else args.behave_options

    clean_repo()

    output_dir = create_output_directory()
    json_out_dir = os.path.join(output_dir, 'json_report_out.json')
    junit_out_dir = os.path.join(output_dir, 'junit_report_out')
    allure_out_dir = os.path.join(output_dir, 'allure_report_out')

    command = f'behave -k -f json.pretty -o "{json_out_dir}" ' \
              f'--junit --junit-directory "{junit_out_dir}" ' \
              f'-f allure_behave.formatter:AllureFormatter -o {allure_out_dir}' \
              f'{behave_options} ' \
              f'{test_dir} '

    print(f"Running command: {command}")

    rs = subprocess.run(command, shell=True)

    print(f"JSON Report directory: {json_out_dir}")
    print(f"JUNIT Report directory: {junit_out_dir}")
    print(f"Allure Report directory: {allure_out_dir}")

    if run_allure and run_allure.upper() == 'TRUE':
        subprocess.run(f'allure serve {allure_out_dir}', shell=True)
