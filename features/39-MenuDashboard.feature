Feature: As a registered user I should be able to add/ remove widget's to dashboard

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able add widgets to my dashboard
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Dashboard Menu From Main Menu
    And I add widget <widget> to  my dashboard
    Then I can see the added <widget> in my dashboard

    Examples:
      | username           | widget          |
      | virendra@ausgridft | Driver Behavior |

  Scenario Outline: As a registered user I should be able to delete widgets from my dashboard
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Dashboard Menu From Main Menu
    And I click on delete button of the widget <widget>
    Then I can not see the deleted <widget> in my dashboard

    Examples:
      | username           | widget          |
      | virendra@ausgridft | Driver Behavior |