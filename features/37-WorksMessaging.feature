Feature: As a registered user I should be able to add new messages and should be able to view the assigned messages

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new message
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Message Menu from Works Menu
    And I click on New message menu from Message Menus
    And I click on Vehicle <vehicle> from the new works vehicle list
    And I added new work with <summary>, <time> and saved
    Then I can see the success message <message> display on placeholder

    Examples:
      | username           | vehicle | summary           | message  | time |
      | virendra@ausgridft | 50062   | Automated Message | Success! | Now  |


  Scenario Outline: As a registered user I should be able to see the added message in the message list
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Message Menu from Works Menu
    And I click on Vehicle <vehicle> from the new works vehicle list
    Then I should be able to added message summary <summary> in the message list

    Examples:
      | username           | vehicle | summary           |
      | virendra@ausgridft | 50062   | Automated Message |