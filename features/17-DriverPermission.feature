Feature: As a registered user I should be able to assign groups to user via permissions

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to assign driver permissions
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and then driver permissions from manage menus
    And I click on the driver name from the driver list <driver>
    And I assigned group <group> from available groups
    Then I can see the assigned group <group> in the assigned groups

    Examples:
      | username           | driver         | group             |
      | virendra@ausgridft | Eranda@Drivers | Demo Driver Group |

  Scenario Outline: As a registered user I should be able to un assign driver permissions
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and then driver permissions from manage menus
    And I click on the driver name from the driver list <driver>
    And I un assigned group <group> from assigned groups
    Then I can see the un assigned group <group> in the available groups

    Examples:
      | username           | driver         | group             |
      | virendra@ausgridft | Eranda@Drivers | Demo Driver Group |