Feature: As a registered user I should be able to add/edit/delete the fuel entries

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add fuel entries
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on tools and then fuel consumption from manage menus
    And I click on vehicle with <name> in vehicle fuel list
    And I click on add group button
    And I added fuel consumption entry with <date> and saved
    Then I can see the added fuel consumption record <date>

    Examples:
      | username           | name          | date               |
      | virendra@ausgridft | Rodney Subaru | 24/03/2021 5:46 PM |

  Scenario Outline: As a registered user I should be able to edit fuel entries
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on tools and then fuel consumption from manage menus
    And I click on vehicle with <name> in vehicle fuel list
    And I changed the fuel consumption record date from <date> to <date2>
    Then I can see the added fuel consumption record <date2>

    Examples:
      | username           | name          | date               | date2              |
      | virendra@ausgridft | Rodney Subaru | 24/03/2021 5:46 PM | 23/03/2021 5:46 PM |

  Scenario Outline: As a registered user I should be able to delete fuel entries
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on tools and then fuel consumption from manage menus
    And I click on vehicle with <name> in vehicle fuel list
    And I delete the fuel consumption record from grid
    Then I can see the added fuel consumption record <date>

    Examples:
      | username           | name          | date               |
      | virendra@ausgridft | Rodney Subaru | 23/03/2021 5:46 PM |
