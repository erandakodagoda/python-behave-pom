############################################################## TNT #################################################
Feature: As a user I should be able to login to the AVM TNT application as

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | tnt         |

  Scenario: As a registered user I should be able to login using valid username and password
    When I navigate to AVM login page
    And I enter username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    And I click on login button
    Then I verify dashboard page appearing with the <username>


  Scenario Outline: As a registered user I should not be able to login using invalid username and password
    When I navigate to AVM login page
    And I enter username and password in:
      | username     | password   |
      | pinpoint@tnt | Pinpoint12 |
    And I click on login button
    Then I verify validation message <validation>

    Examples:
      | validation                   |
      | Invalid username or password |