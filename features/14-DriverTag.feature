Feature: As a registered user I should be able to add tags

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add Tags
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Driver menu and navigated to tags
    And I enter Driver RF Tag <tag> and saved
    Then I can see the added tag <tag> in the list

    Examples:
      | username           | tag                               |
      | virendra@ausgridft | 724683947934739579457396832A7932G |
