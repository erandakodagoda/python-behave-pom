Feature: As a logged in user I should be able to see Menus in the Dashboard

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | tnt         |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab

    Examples:
      | username     | group     |
      | pinpoint@tnt | Ungrouped |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu and select a vehicle
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab
    And I click on vehicle from the list <vehicleId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver | assignedDriver |
      | 007070      |               |                |

    Examples:
      | username     | group     | vehicleId |
      | pinpoint@tnt | Ungrouped | 007070    |

#  Scenario Outline: As a registered user I should be able to navigate to Driver Menu
#    When I login to the application using username and password in:
#      | username     | password  |
#      | pinpoint@tnt | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the driver tab
#
#    Examples:
#      | username     | run             |
#      | pinpoint@tnt | 3 Private Works |
#
#  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
#    When I login to the application using username and password in:
#      | username     | password  |
#      | pinpoint@tnt | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the driver tab
#    And I click on run from the list <runId>
#    And I should be able to see the vehicle details:
#      | vehicleName  | currentDriver        | assignedDriver | driverStatus |
#      | T1123 FS Nth | Mark Longhurst POE B |                | Logged in    |
#
#    Examples:
#      | username     | run             | runId          |
#      | pinpoint@tnt | 3 Private Works | Gregory Single |

  Scenario Outline: As a registered user I should be able to navigate to Runs Menu
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the run tab

    Examples:
      | username     | run |
      | pinpoint@tnt | ABX |

  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the run tab
    And I click on run group from the list <runId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver | assignedDriver | driverStatus | run   |
      | 007070      |               |                | Logged off   | 26001 |

    Examples:
      | username     | run | runId |
      | pinpoint@tnt | ABX | 26001 |

  Scenario Outline: As a registered User I should be able to see the Locations Menu and select a location
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <location> under the location tab
    And I click on location from the list <location>
    And I should be able to see the location details:
      | locationName | locationGroup | locationType | locationDepart | isPending |
      | TNT Redbank  | Ungrouped     | TNT Depot    | BS7            | No        |

    Examples:
      | username     | location    |
      | pinpoint@tnt | TNT Redbank |

  Scenario Outline: As a registered User I should be able to see the Alert Menu and select an alert
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alert> under the alert tab
    And I click on alert from the list <carId>
    And I should be able to see the alert details:
      | alertMessage | alertVehicle | alertFleetId |
      | Speeding     | 006649       | 4086         |

    Examples:
      | username     | carId  | alert    |
      | pinpoint@tnt | 006649 | Speeding |

  Scenario Outline: As a registered User I should be able to see the Alarm Menu and select an alarm
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@tnt | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alarm> under the alarm tab
    And I click on alarm from the list <carId>
    And I should be able to see the alert details:
      | alertMessage              | alertVehicle                  | alertFleetId |
      | High Speed Crash Detected | GPS 1428 # Nintendo SYD - MEL | 1428         |

    Examples:
      | username     | carId                         | alarm                     |
      | pinpoint@tnt | GPS 1428 # Nintendo SYD - MEL | High Speed Crash Detected |
