Feature: As  a registered user i shouls be able to manage the assignments of vehicles towards driver

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to assign drivers to vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and then manage assignment from manage menus
    And I click on the vehicle <vehicle> and driver <driver> from list
    And I click on add button in the center to assign
    Then I can see the assigned driver <driver> in the vehicle assignment list

    Examples:
      | username           | driver      | vehicle   |
      | virendra@ausgridft | Demo@Driver | WifiTest5 |