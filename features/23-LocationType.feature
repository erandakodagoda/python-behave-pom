Feature: As a registered user I should be able to add/Edit/remove new Location Type

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new Location Type
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on location and then type from manage menus
    And I click on add button from run
    And I added location type <name> and saved
    Then I can see the new location type <name> in the list

    Examples:
      | username           | name               |
      | virendra@ausgridft | Demo Location Type |

  Scenario Outline: As a registered User I should be able to edit the run type
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on location and then type from manage menus
    And I click on the location type <name> from the list
    And I added location type <name2> and saved
    Then I can see the new location type <name2> in the list

    Examples:
      | username           | name               | name2                  |
      | virendra@ausgridft | Demo Location Type | Demo Location Type New |

  Scenario Outline: As a registered user I should be able to delete location type
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on location and then type from manage menus
    And I click on the location type <name> from the list
    And I click on delete button
    Then I can not see the new location type <name> in the list

    Examples:
      | username           | name                   |
      | virendra@ausgridft | Demo Location Type New |
