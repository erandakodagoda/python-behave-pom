Feature: As a logged in user I should be able to see Menus in the Dashboard

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | aus3        |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab

    Examples:
      | username         | group    |
      | pinpoint@redflex | Thornton |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu and select a vehicle
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab
    And I click on vehicle from the list <vehicleId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver  | assignedDriver |
      | M201-091    | Gregory Single |                |

    Examples:
      | username         | group    | vehicleId |
      | pinpoint@redflex | Thornton | CP61PF    |

  Scenario Outline: As a registered user I should be able to navigate to Driver Menu
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab

    Examples:
      | username         | run       |
      | pinpoint@redflex | Ungrouped |

  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab
    And I click on run from the list <runId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver  | assignedDriver | driverStatus |
      | M201-091    | Gregory Single |                | Logged in    |

    Examples:
      | username         | run       | runId          |
      | pinpoint@redflex | Ungrouped | Gregory Single |

#  Scenario Outline: As a registered user I should be able to navigate to Runs Menu
#    When I login to the application using username and password in:
#      | username           | password  |
#      | pinpoint@kennards | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#
#    Examples:
#      | username           | run        |
#      | virendra@ausgridft | Eranda-Run |
#
#  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
#    When I login to the application using username and password in:
#      | username           | password  | environment |
#      | virendra@ausgridft | Pinpoint1 | uat         |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#    And I click on run group from the list <runId>
#    And I should be able to see the vehicle details:
#      | vehicleName | currentDriver | assignedDriver | driverStatus | run           |
#      | WifiTest5   | Eranda Driver | Eranda Driver  | Logged in    | ErandaTestRun |
#
#    Examples:
#      | username           | run               | runId         |
#      | virendra@ausgridft | Eranda- Run Group | ErandaTestRun |

  Scenario Outline: As a registered User I should be able to see the Locations Menu and select a location
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <location> under the location tab
    And I click on location from the list <location>
    And I should be able to see the location details:
      | locationName              | locationGroup | locationType | locationDepart | isPending |
      | 10004 SANDY HOLLOW - 2333 | POI           | Site         | No department  | No        |

    Examples:
      | username         | location                  |
      | pinpoint@redflex | 10004 SANDY HOLLOW - 2333 |

  Scenario Outline: As a registered User I should be able to see the Alert Menu and select an alert
    When I login to the application using username and password in:
      | username         | password  |
      | pinpoint@redflex | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alert> under the alert tab
    And I click on alert from the list <carId>
    And I should be able to see the alert details:
      | alertMessage  | alertVehicle | alertFleetId |
      | Emergency Set | CP71PG       | 29163        |

    Examples:
      | username         | carId  | alert         |
      | pinpoint@redflex | CP71PG | Emergency Set |

#  Scenario Outline: As a registered User I should be able to see the Alarm Menu and select an alarm
#    When I login to the application using username and password in:
#      | username          | password  |
#      | pinpoint@kennards | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <alarm> under the alarm tab
#    And I click on alarm from the list <carId>
#    And I should be able to see the alert details:
#      | alertMessage | alertVehicle | alertFleetId |
#      | BreakDown    | 9095         | 9095         |
#
#    Examples:
#      | username          | carId | alarm     |
#      | pinpoint@kennards | 9095  | BreakDown |
