Feature: As a registered user I should be able to add employee

  Scenario Outline: As a user I should be able to pass GET request to employees
    When I am sending a GET request to <endpoint> and I assert the response <response>

    Examples:
      | endpoint    | response    |
      | /employee/1 | Tiger Nixon |