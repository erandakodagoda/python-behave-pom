Feature: As a registered user I should be able to add new run groups and assign runs to groups

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new run groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then groups from manage menus
    And I click on add group button
    And I add a run group with <name> and saved
    Then I can see driver group <name> in the run group list

    Examples:
      | username           | name           |
      | virendra@ausgridft | Demo Run Group |

  Scenario Outline: As a registered user I should be able assign runs to group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then groups from manage menus
    And I select run group <group> from run group list
    And I added run <run> from right panel
    Then I can see the assigned run <run> in assigned run list

    Examples:
      | username           | run         | group          |
      | virendra@ausgridft | TestRun2309 | Demo Run Group |

  Scenario Outline: As a registered user I should be able to un assign driver from group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then groups from manage menus
    And I select run group <group> from run group list
    And I unassigned run <run> from left panel
    Then I can see the un assigned run <run> in available run list

    Examples:
      | username           | run         | group          |
      | virendra@ausgridft | TestRun2309 | Demo Run Group |

  Scenario Outline: As a registered user I should be able to edit run groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then groups from manage menus
    And I select run group <name> from run group list
    And I click on edit button
    And I add a run group with <name2> and saved
    Then I can see driver group <name2> in the run group list

    Examples:
      | username           | name           | name2      |
      | virendra@ausgridft | Demo Run Group | Demo Run G |

  Scenario Outline: As a registered user I should be able to delete run groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then groups from manage menus
    And I select run group <name> from run group list
    And I click on delete button
    Then I can not see driver group <name> in the run group list

    Examples:
      | username           | name       |
      | virendra@ausgridft | Demo Run G |
