Feature: As a registered user I should be able to assign and un assign permission to Runs

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to assign permissions to runs
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then permission from manage menus
    And I click on driver name <driver> from the run list
    And I assigned a run permission group with <name> and saved
    Then I can see assigned run group <name> in the run group list

    Examples:
      | username           | name           | driver         |
      | virendra@ausgridft | Demo Run Group | Eranda@Drivers |

  Scenario Outline: As a registered user I should be able to un assign permissions to runs
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then permission from manage menus
    And I click on driver name <driver> from the run list
    And I un assigned a run permission group with <name> and saved
    Then I can see un assigned run group <name> in the run group list

    Examples:
      | username           | name           | driver         |
      | virendra@ausgridft | Demo Run Group | Eranda@Drivers |