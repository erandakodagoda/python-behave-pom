Feature: As a registered user I should be able to add/edit/delete contact details

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |


  Scenario Outline: As a registered User I should be able to add contact groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on contacts and then details from manage menus
    And I click on add group button
    And I enter contact group <name> and saved
    Then I can see the added contact group <name> in the list

    Examples:
      | username           | name               |
      | virendra@ausgridft | Test Contact Group |

  Scenario Outline: As a registered user I should be able to add contacts to groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on contacts and then details from manage menus
    And I click on a contact group <group> from the list
    And I click on add button in the grid
    And I added contact with <name>, <mobile>, <email> and saved
    Then I can see the added name <name> in the grid

    Examples:
      | username           | group              | name   | mobile       | email                     |
      | virendra@ausgridft | Test Contact Group | Eranda | +94763040064 | erandak@netstaraus.com.au |


  Scenario Outline: As a registered user I should be able to delete contacts from groups
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on contacts and then details from manage menus
    And I click on a contact group <group> from the list
    And I click on delete button and saved
    Then I can see save success validation

    Examples:
      | username           | group              |
      | virendra@ausgridft | Test Contact Group |

  Scenario Outline: As a registered user I should be able to delete contact group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on contacts and then details from manage menus
    And I click on a contact group <group> from the list
    And I click on delete button
    Then I can not see the deleted group <group> in the list

    Examples:
      | username           | group              |
      | virendra@ausgridft | Test Contact Group |