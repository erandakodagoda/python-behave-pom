Feature: As a registered user I should be able to create/change the user roles and rights

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new users roles and rights
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to roles and rights
    And I click on add button to add roles
    And I added a new role with <role_name> and <all_vehicles>, <all_drivers>
    Then I can see added <role_name> in the list

    Examples:
      | username           | role_name | all_vehicles | all_drivers |
      | virendra@ausgridft | New Role  | Yes          | Yes         |

  Scenario Outline: As a registered user I should be able to edit existing users roles and rights
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to roles and rights
    And I click on <role_name> from roles list
    Then I select <permission> from roles and rights

    Examples:
      | username           | role_name | permission    |
      | virendra@ausgridft | New Role  | Functionality |

  Scenario Outline: As a registered user I should be able to delete the user roles and rights
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to roles and rights
    And I click on <role_name> from roles list
    And I click on delete button
    Then I can not see the role <role_name> in the list

    Examples:
      | username           | role_name |
      | virendra@ausgridft | New Role  |