Feature: As a registered user I should be able to see the driver added checklists in the checklist menu

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to view works checklist
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Checklist Menu from Works Menu
    And I click on Vehicle <vehicle> from the vehicle list in works
    And I click on the work record with date <date>
    Then I can see the checklist details as per <history_date>, <status>
    And I navigated back to works list page

    Examples:
      | username           | vehicle | date                   | history_date          | status     |
      | virendra@ausgridft | 50062   | 23/02/2021 11:13:39 AM | 8/02/2021 12:02:57 PM | Safe to go |
