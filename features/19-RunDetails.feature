Feature: As a registered user I should be able to add new run and should be able to see the added run

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add run from manage menus
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then details from manage menus
    And I click on add button from run
    And I add a run with <name>
    Then I can see the added run in the <name> list

    Examples:
      | username           | name    |
      | virendra@ausgridft | DemoRun |

  Scenario Outline: As a registered user I should be able to edit run from manage menus
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then details from manage menus
    And I click on the run <name> from run list
    And I click on edit button
    And I add a run with <name2>
    Then I can see the added run in the <name2> list

    Examples:
      | username           | name    | name2    |
      | virendra@ausgridft | DemoRun | DemoRun2 |

  Scenario Outline: As a registered user I should be able to delete run from manage menus
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then details from manage menus
    And I click on the run <name> from run list
    And I click on delete button
    Then I can not see the deleted run in the <name> list

    Examples:
      | username           | name     |
      | virendra@ausgridft | DemoRun2 |