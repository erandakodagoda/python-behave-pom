Feature: As a logged in user I should be able to see Menus in the Dashboard

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | ivms        |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab

    Examples:
      | username       | group     |
      | pinpoint@adams | Ungrouped |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu and select a vehicle
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab
    And I click on vehicle from the list <vehicleId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver  | assignedDriver |
      | 173VRN      | William Morgan |                |

    Examples:
      | username       | group     | vehicleId |
      | pinpoint@adams | Ungrouped | 16616     |

  Scenario Outline: As a registered user I should be able to navigate to Driver Menu
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab

    Examples:
      | username       | run       |
      | pinpoint@adams | Ungrouped |

  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab
    And I click on run from the list <runId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver  | assignedDriver | driverStatus |
      | 173VRN      | William Morgan |                | Identified   |

    Examples:
      | username       | run       | runId          |
      | pinpoint@adams | Ungrouped | William Morgan |

#  Scenario Outline: As a registered user I should be able to navigate to Runs Menu
#    When I login to the application using username and password in:
#      | username           | password  |
#      | pinpoint@kennards | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#
#    Examples:
#      | username           | run        |
#      | virendra@ausgridft | Eranda-Run |
#
#  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
#    When I login to the application using username and password in:
#      | username           | password  | environment |
#      | virendra@ausgridft | Pinpoint1 | uat         |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#    And I click on run group from the list <runId>
#    And I should be able to see the vehicle details:
#      | vehicleName | currentDriver | assignedDriver | driverStatus | run           |
#      | WifiTest5   | Eranda Driver | Eranda Driver  | Logged in    | ErandaTestRun |
#
#    Examples:
#      | username           | run               | runId         |
#      | virendra@ausgridft | Eranda- Run Group | ErandaTestRun |

  Scenario Outline: As a registered User I should be able to see the Locations Menu and select a location
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <location> under the location tab
    And I click on location from the list <location>
    And I should be able to see the location details:
      | locationName | locationGroup | locationType | locationDepart | isPending |
      | TARCOOLA     | Tarcoola      | Tarcoola     | No department  | No        |

    Examples:
      | username       | location |
      | pinpoint@adams | TARCOOLA |

  Scenario Outline: As a registered User I should be able to see the Alert Menu and select an alert
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alert> under the alert tab
    And I click on alert from the list <carId>
    And I should be able to see the alert details:
      | alertMessage    | alertVehicle | alertFleetId |
      | No Driver Login | 16611        | 16611        |

    Examples:
      | username       | carId | alert           |
      | pinpoint@adams | 16611 | No Driver Login |

  Scenario Outline: As a registered User I should be able to see the Alarm Menu and select an alarm
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@adams | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alarm> under the alarm tab
    And I click on alarm from the list <carId>
    And I should be able to see the alert details:
      | alertMessage      | alertVehicle | alertFleetId |
      | Rollover Detected | 23516        | 23516        |

    Examples:
      | username       | carId | alarm             |
      | pinpoint@adams | 23516 | Rollover Detected |
