Feature: As a registered user I should be able to add permission to driver user

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add permission to a driver
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to permission
    And I select <driver> from the list
    And I added group <group> from right panel
    Then I can see the <group> in the left panel

    Examples:
      | username           | driver         | group   |
      | virendra@ausgridft | Eranda@Drivers | Group A |

  Scenario Outline: As a registered user I should be able to remove permission to a driver to group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to permission
    And I select <driver> from the list
    And I removed group <group> from left panel
    Then I can see the <vehicle> in the left panel

    Examples:
      | username           | driver         | group   |
      | virendra@ausgridft | Eranda@Drivers | Group A |