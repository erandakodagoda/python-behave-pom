Feature: As a registered user I should be able to see All Activity report and Vehicle Trip report

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | aus2        |

  Scenario Outline: As a user I should be able to validate vehicle all activity report
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Reports Menu from main Menu
    And I select Vehicles Menu from Reports Menus
    And I select filters from reports:
      | filterType | reportType | reportName   |
      | Individual | Detailed   | All Activity |
    And I select vehicles from left grid list:
      | vehicleId          |
      | (PRIVATE) Ben Loss |
    And I select the date range <startDate> and <endDate>
    Then I click on Generate button
    And I should see report with vehicles:
      | reportVehicleId              |
      | Activity report for Ben Loss |

    Examples:
      | username          | startDate          | endDate            |
      | pinpoint@kennards | 12/12/2020 6:30 PM | 19/12/2020 6:30 PM |


  Scenario Outline: As a user I should be able to validate vehicle trip report
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Reports Menu from main Menu
    And I select Vehicles Menu from Reports Menus
    And I select filters from reports:
      | filterType | reportType | reportName |
      | Individual | Detailed   | Trip       |
    And I select vehicles from left grid list:
      | vehicleId          |
      | (PRIVATE) Ben Loss |
    And I select the date range <startDate> and <endDate>
    Then I click on Generate button
    And I should see report with vehicles:
      | reportVehicleId          |
      | Trip report for Ben Loss |

    Examples:
      | username          | startDate          | endDate            |
      | pinpoint@kennards | 12/12/2020 6:30 PM | 19/12/2020 6:30 PM |

