Feature: As a registered user I should be bale to view the Works data in Work List

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to view works list
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on Vehicle <vehicle> from the vehicle list in works
    And I click on the work record with date <date>
    Then I can see the work details as per <date>, <vehicle_name>

    Examples:
      | username           | vehicle | date                  | vehicle_name  |
      | virendra@ausgridft | 50062   | 9/02/2021 11:06:03 AM | TRK2004 50062 |

  Scenario Outline: As a registered user I should be able to add Work from Work List Screen
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on Vehicle <vehicle> from the vehicle list in works
    And I click on add new work button
    And I added new work with <summary> and saved
    Then I can see the success message <message> display on placeholder

    Examples:
      | username           | vehicle | summary             | message  |
      | virendra@ausgridft | 50062   | Test Demo Work List | Success! |