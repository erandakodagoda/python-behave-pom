Feature: As a registered user I should be able to allocate/remove allocation/delete works

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to remove allocation dispatch
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Dispatch Menu from Works Menu
    And I click on Vehicle <vehicle> from the new works vehicle list
    And I click on <summary> from allocated and click on deallocate button
    Then I can see the <summary> in the allocatable section
    And I navigated back to works list page

    Examples:
      | username           | vehicle | summary            |
      | virendra@ausgridft | 50062   | Automation Summary |

  Scenario Outline: As a registered user I should be able to allocate to dispatch
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Dispatch Menu from Works Menu
    And I click on Vehicle <vehicle> from the new works vehicle list
    And I click on <summary> from available allocation and click on allocate button
    Then I can see the <summary> in the allocated section
    And I navigated back to works list page

    Examples:
      | username           | vehicle | summary            |
      | virendra@ausgridft | 50062   | Automation Summary |

  Scenario Outline: As a registered user I should be able to delete work from dispatch
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Dispatch Menu from Works Menu
    And I click on Vehicle <vehicle> from the new works vehicle list
    And I delete the work <summary> from allocated list
    Then I can not see the <summary> in the allocated list
    And I navigated back to works list page

    Examples:
      | username           | vehicle | summary            |
      | virendra@ausgridft | 50062   | Automation Summary |


  Scenario Outline: As a registered user I should be able to add Work from Dispatch Screen
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the Dispatch Menu from Works Menu
    And I click on Vehicle <vehicle> from the vehicle list in works
    And I click on add new work button
    And I added new work with <summary> and saved
    Then I can see the success message <message> display on placeholder
    And I navigated back to works list page

    Examples:
      | username           | vehicle | summary             | message  |
      | virendra@ausgridft | 50062   | Test Demo Work List | Success! |


