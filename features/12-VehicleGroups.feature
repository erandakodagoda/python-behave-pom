Feature: As a registered user I should be able to add new groups and assign vehicles to groups

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to groups
    And I click on add group button
    And I enter group <name> and saved
    Then I can see group <name> in the group list

    Examples:
      | username           | name       |
      | virendra@ausgridft | Demo Group |

  Scenario Outline: As a registered user I should be able assign vehicles to group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to groups
    And I select group <group> from group list
    And I added vehicle <vehicle> from right panel
    Then I can see the assigned vehicle <vehicle> in assigned list

    Examples:
      | username           | vehicle     | group      |
      | virendra@ausgridft | 61478761790 | Demo Group |

  Scenario Outline: As a registered user I should be able to un assign vehicles from group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to groups
    And I select group <group> from group list
    And I removed vehicle <vehicle> from right panel
    Then I can see the unassigned vehicle <vehicle> in available list

    Examples:
      | username           | vehicle     | group      |
      | virendra@ausgridft | 61478761790 | Demo Group |


  Scenario Outline: As a registered user I should be able to edit group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to groups
    And I select group <name> from group list
    And I click on edit button
    And I enter group <name2> and saved
    Then I can see group <name2> in the group list

    Examples:
      | username           | name       | name2        |
      | virendra@ausgridft | Demo Group | Demo Group 2 |

  Scenario Outline: As a registered user I should be able to delete group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to groups
    And I select group <name> from group list
    And I click on delete button
    Then I can not see group <name> in the group list

    Examples:
      | username           | name         |
      | virendra@ausgridft | Demo Group 2 |
