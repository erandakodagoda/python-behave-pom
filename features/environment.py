from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from config.config import Config


def before_scenario(context, scenario, browser_type=Config.BROWSER_TYPE):
    if not browser_type:
        context.driver = webdriver.Chrome(executable_path=Config.DRIVERS[0].get("path"))
        context.driver.maximize_window()
    elif browser_type.lower() == 'chrome':
        options = Options()
        # options.add_argument('--headless')
        context.driver = webdriver.Chrome(executable_path=Config.DRIVERS[0].get("path"), chrome_options=options)
        context.driver.maximize_window()
    elif browser_type.lower() == 'firefox':
        context.driver = webdriver.Firefox(executable_path=Config.DRIVERS[1].get("path"))
        context.driver.maximize_window()
    elif browser_type.lower() == 'ie':
        context.driver = webdriver.Ie(executable_path=Config.DRIVERS[2].get("path"))
    elif browser_type.lower() == 'safari':
        context.driver = webdriver.Safari(executable_path=Config.DRIVERS[3].get("path"))
    else:
        raise Exception("Browser type {} is not found".format(browser_type))

    return context.driver


def after_scenario(context, scenario):
    context.driver.close()
    context.driver.quit()
