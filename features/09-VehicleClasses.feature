Feature: As a registered user I should be able to add Vehicle classes

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add vehicle classes
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to classes
    And I click on add new class button
    And I added new vehicle class with <name>
    Then I can see <name> in the class list


    Examples:
      | username           | name       |
      | virendra@ausgridft | Demo Class |

  Scenario Outline: As a registered user I should be able to edit vehicle classes
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to classes
    And I click on vehicle class <name> from the list
    And I added new vehicle class with <name2>
    Then I can see <name2> in the class list


    Examples:
      | username           | name       | name2        |
      | virendra@ausgridft | Demo Class | Demo Class 2 |

  Scenario Outline: As a registered user I should be able to delete vehicle classes
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to classes
    And I click on vehicle class <name> from the list
    And I click on delete button
    Then I can not see <name> in the class list


    Examples:
      | username           | name         |
      | virendra@ausgridft | Demo Class 2 |

