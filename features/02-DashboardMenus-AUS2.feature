Feature: As a logged in user I should be able to see Menus in the Dashboard

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | aus2        |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab

    Examples:
      | username     | group        |
      | pinpoint@cca | Jared Cannon |

  Scenario Outline: As a registered User I should be able to see the Vehicles Menu and select a vehicle
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <group> under the vehicle tab
    And I click on vehicle from the list <vehicleId>
    And I should be able to see the vehicle details:
      | vehicleName | currentDriver            | assignedDriver           |
      | Spare       | SNTHON CO John Delessert | SNTHON CO John Delessert |

    Examples:
      | username     | group        | vehicleId |
      | pinpoint@cca | Jared Cannon | CS19WK    |

  Scenario Outline: As a registered user I should be able to navigate to Driver Menu
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab

    Examples:
      | username     | run       |
      | pinpoint@cca | Ungrouped |

  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <run> under the driver tab
    And I click on run from the list <runId>
    And I should be able to see the vehicle details:
      | vehicleName   | currentDriver | assignedDriver | driverStatus |
      | 2005/EFLD0130 | Harvey Nash   | Harvey Nash    | Identified   |

    Examples:
      | username     | run       | runId       |
      | pinpoint@cca | Ungrouped | Harvey Nash |

#  Scenario Outline: As a registered user I should be able to navigate to Runs Menu
#    When I login to the application using username and password in:
#      | username           | password  |
#      | pinpoint@kennards | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#
#    Examples:
#      | username           | run        |
#      | virendra@ausgridft | Eranda-Run |
#
#  Scenario Outline: As a registered User I should be able to see the Driver Runs Menu and select a run
#    When I login to the application using username and password in:
#      | username           | password  | environment |
#      | virendra@ausgridft | Pinpoint1 | uat         |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <run> under the run tab
#    And I click on run group from the list <runId>
#    And I should be able to see the vehicle details:
#      | vehicleName | currentDriver | assignedDriver | driverStatus | run           |
#      | WifiTest5   | Eranda Driver | Eranda Driver  | Logged in    | ErandaTestRun |
#
#    Examples:
#      | username           | run               | runId         |
#      | virendra@ausgridft | Eranda- Run Group | ErandaTestRun |

  Scenario Outline: As a registered User I should be able to see the Locations Menu and select a location
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <location> under the location tab
    And I click on location from the list <location>
    And I should be able to see the location details:
      | locationName        | locationGroup | locationType              | locationDepart | isPending |
      | Kingsley Drop Point | WA Locations  | Main Type (Do not delete) | WA             | No        |

    Examples:
      | username     | location            |
      | pinpoint@cca | Kingsley Drop Point |

  Scenario Outline: As a registered User I should be able to see the Alert Menu and select an alert
    When I login to the application using username and password in:
      | username     | password  |
      | pinpoint@cca | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    And I should be able to see the <alert> under the alert tab
    And I click on alert from the list <carId>
    And I should be able to see the alert details:
      | alertMessage | alertVehicle | alertFleetId |
      | Power Fail   | CV25MR       | 37908        |

    Examples:
      | username     | carId  | alert      |
      | pinpoint@cca | CV25MR | Power Fail |

#  Scenario Outline: As a registered User I should be able to see the Alarm Menu and select an alarm
#    When I login to the application using username and password in:
#      | username          | password  |
#      | pinpoint@kennards | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    And I should be able to see the <alarm> under the alarm tab
#    And I click on alarm from the list <carId>
#    And I should be able to see the alert details:
#      | alertMessage | alertVehicle | alertFleetId |
#      | BreakDown    | 9095         | 9095         |
#
#    Examples:
#      | username          | carId | alarm     |
#      | pinpoint@kennards | 9095  | BreakDown |
