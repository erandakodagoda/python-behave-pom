Feature: As a registered user I should be able to add/edit/delete department

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new department
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on department and then details from manage menus
    And I click on add group button
    And I enter department <name> and saved
    Then I can see the added department <name> in the list

    Examples:
      | username           | name            |
      | virendra@ausgridft | Test Department |

  Scenario Outline: As a registered user I should be able to edit new department
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on department and then details from manage menus
    And I click on department <name> from list
    Then I update alerts and added vehicle alerts to <contact> contact group

    Examples:
      | username           | name            | contact    |
      | virendra@ausgridft | Test Department | QA Contact |


  Scenario Outline: As a registered user I should be able to delete the department
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on department and then details from manage menus
    And I click on department <name> from list
    And I click on delete button
    Then I can not see the department <name> in the list

    Examples:
      | username           | name            |
      | virendra@ausgridft | Test Department |