Feature: As a registered user I should be able to edit and view vehicle details

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to view vehicle details
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to details
    And I click on vehicle <vehicle> from the vehicle list
    Then I verify vehicle <vehicle> appears in the text field

    Examples:
      | username           | vehicle   |
      | virendra@ausgridft | WifiTest5 |

  Scenario Outline: As a registered user I should be able to edit vehicle details
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to details
    And I click on vehicle <vehicle> from the vehicle list
    And I edit the vehicle id <vehicle_id> to selected vehicle and saved
    Then I verify vehicle id <vehicle_id> on field

    Examples:
      | username           | vehicle   | vehicle_id |
      | virendra@ausgridft | WifiTest5 | 11         |