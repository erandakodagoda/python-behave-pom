Feature: As a registered user I should be able to see All Activity report and Vehicle Trip report

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | unitywater  |

  Scenario Outline: As a user I should be able to validate vehicle all activity report
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@unity | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Reports Menu from main Menu
    And I select Vehicles Menu from Reports Menus
    And I select filters from reports:
      | filterType | reportType | reportName   |
      | Individual | Detailed   | All Activity |
    And I select vehicles from left grid list:
      | vehicleId     |
      | V1635 Tech DS |
    And I select the date range <startDate> and <endDate>
    Then I click on Generate button
    And I should see report with vehicles:
      | reportVehicleId       |
      | Vehicle V1635 Tech DS |

    Examples:
      | username       | startDate          | endDate           |
      | pinpoint@adams | 26/02/2020 7:41 AM | 3/03/2020 8:41 AM |


  Scenario Outline: As a user I should be able to validate vehicle trip report
    When I login to the application using username and password in:
      | username       | password  |
      | pinpoint@unity | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Reports Menu from main Menu
    And I select Vehicles Menu from Reports Menus
    And I select filters from reports:
      | filterType | reportType | reportName |
      | Individual | Detailed   | Trip       |
    And I select vehicles from left grid list:
      | vehicleId     |
      | V1635 Tech DS |
    And I select the date range <startDate> and <endDate>
    Then I click on Generate button
    And I should see report with vehicles:
      | reportVehicleId               |
      | Trip report for V1635 Tech DS |

    Examples:
      | username       | startDate          | endDate           |
      | pinpoint@adams | 26/02/2020 7:41 AM | 3/03/2020 8:41 AM |

