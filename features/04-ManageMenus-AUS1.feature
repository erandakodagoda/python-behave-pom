Feature: As a registered user I should be able to view Manage Menus

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | aus1        |

  Scenario Outline: As a registered user I should be able to see Manage Menu Drivers Detail
    When I login to the application using username and password in:
      | username          | password  |
      | pinpoint@kennards | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Drivers and then Driver Details from Manage Menus
    Then I should be able to see the Title of Menu Page <title>


    Examples:
      | username          | title          |
      | pinpoint@kennards | Driver details |

  Scenario Outline: As a registered user I should be able to see Manage Menu Vehicle Detail
    When I login to the application using username and password in:
      | username          | password  |
      | pinpoint@kennards | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles and then Vehicle Details from Manage Menus
    Then I should be able to see the Title of Menu Page <title>


    Examples:
      | username           | title           |
      | pinpoint@kennards | Vehicle details |