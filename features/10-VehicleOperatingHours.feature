Feature: As a registered user I should be able to add operating hours

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |


  Scenario Outline: As a registered user I should be able to add operating hours
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to operating hours
    And I click on add operating hours button
    And I add operating hours entry with <name>
    Then I can see the added <name> in the grid

    Examples:
      | username           | name       |
      | virendra@ausgridft | Demo Hours |


  Scenario Outline: As a registered user I should be able to edit operating hours
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to operating hours
    And I click on the operating hours <name> in the grid
    And I click on edit button
    And I add operating hours entry with <name2>
    Then I can see the added <name2> in the grid

    Examples:
      | username           | name       | name2           |
      | virendra@ausgridft | Demo Hours | Demo Hours Test |


  Scenario Outline: As a registered user I should be able to delete operating hours
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to operating hours
    And I click on the operating hours <name> in the grid
    And I click on delete button
    Then I can not see the added <name> in the grid

    Examples:
      | username           | name            |
      | virendra@ausgridft | Demo Hours Test |