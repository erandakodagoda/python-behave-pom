Feature: As a registered user I should be able to create new work

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new works
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Works Menu from main menu
    And I click on the New Works Menu from Works Menu
    And I click on Vehicle <vehicle> from the new works vehicle list
    And I added Works to the Vehicle with <summary> for today and saved
    Then I can see the assigned new work <summary> in the grid
    And I navigated back to works list page

    Examples:
      | username           | vehicle | summary            |
      | virendra@ausgridft | 50062   | Automation Summary |

