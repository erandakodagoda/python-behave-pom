from behave import when, then

from pages.manage_contact_page import ContactManagementPage
from pages.manage_menu_page import ManagePage


class ContactManagementSteps:
    @when("I click on contacts and then details from manage menus")
    def i_click_on_contacts_details(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_contacts_details()

    @then("I can see the added contact group {name} in the list")
    def i_can_see_the_contact_group(context, name):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.verify_visibility_in_list(name=name)

    @when("I click on a contact group {group} from the list")
    def i_click_on_contact_group(context, group):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.click_on_contact_in_group(name=group)

    @when("I click on add button in the grid")
    def i_click_on_add_button_in_grid(context):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.click_on_add_button_in_grid()

    @when("I added contact with {name}, {mobile}, {email} and saved")
    def i_added_contact(context, name, mobile, email):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.add_contact_to_group(name=name, mobile=mobile, email=email)

    @then("I can see the added name {name} in the grid")
    def i_can_see_the_added_contact(context, name):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.verify_visibility_of_contact(name=name)

    @when("I enter contact group {name} and saved")
    def i_enter_contact_detail_and_saved(context, name):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.add_contact_group(name=name)

    @then("I can see save success validation")
    def i_can_not_see_the_added_contact(context):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.verify_visibility_of_contact()

    @when("I click on delete button and saved")
    def delete_contact_and_save(context):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.delete_contact_from_group()

    @then("I can not see the deleted group {group} in the list")
    def i_can_not_see_the_group(context, group):
        context.manage_contacts = ContactManagementPage(context.driver)
        context.manage_contacts.verify_visibility_in_list(name=group)
