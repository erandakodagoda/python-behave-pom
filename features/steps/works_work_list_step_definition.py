from behave import when, then

from pages.work_list_page import WorkListPage


class WorkListStepDefinition:
    @when("I click on Vehicle {vehicle} from the vehicle list in works")
    def i_click_on_vehicle_from_list(context, vehicle):
        context.works_list = WorkListPage(context.driver)
        context.works_list.select_vehicle_from_list(vehicle)

    @when("I click on the work record with date {date}")
    def i_click_on_the_record_with_date(context, date):
        context.works_list = WorkListPage(context.driver)
        context.works_list.click_on_work_list_record(date)

    @then("I can see the work details as per {date}, {vehicle_name}")
    def i_can_see_work_details(context, date, vehicle_name):
        context.works_list = WorkListPage(context.driver)
        context.works_list.validate_work_list_entry(date, vehicle_name)

    @when("I click on the Checklist Menu from Works Menu")
    def click_on_checklist_menu(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_to_checklist()

    @then("I can see the checklist details as per {history_date}, {status}")
    def i_can_see_the_checklist_record_as_per(context, history_date, status):
        context.works_list = WorkListPage(context.driver)
        context.works_list.validate_the_checklist_entry(history_date, status)

    @then("I navigated back to works list page")
    def i_navigate_back_to_works_list_page(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_back_to_work_list()

    @when("I click on the New Works Menu from Works Menu")
    def i_click_on_new_works_menu(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_to_new_works()

    @when("I added Works to the Vehicle with {summary} for today and saved")
    def i_added_works_to_the_vehicle_for_today(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.add_new_work(summary)

    @then("I can see the assigned new work {summary} in the grid")
    def i_can_see_the_added_work_in_the_grid(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_added_work(summary)

    @when("I click on Vehicle {vehicle} from the new works vehicle list")
    def i_click_on_vehicle_from_work_list(context, vehicle):
        context.works_list = WorkListPage(context.driver)
        context.works_list.select_vehicle_from_work_list(vehicle)

    @when("I click on the Dispatch Menu from Works Menu")
    def i_click_on_dispatch_and_navigated(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_to_dispatch()

    @when("I click on {summary} from allocated and click on deallocate button")
    def i_click_on_summary_and_deallocate(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.deallocate_work_from_vehicle(summary)

    @then("I can see the {summary} in the allocatable section")
    def i_can_see_the_summary_in_allocatable_section(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_deallocated_work_in_available(summary)

    @when("I click on {summary} from available allocation and click on allocate button")
    def i_click_on_summary_and_allocated_to_vehicle(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.allocate_work_to_vehicle(summary)

    @then("I can see the {summary} in the allocated section")
    def i_can_see_the_summary_in_the_allocated_section(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_allocated_work_in_available(summary)

    @when("I delete the work {summary} from allocated list")
    def I_delete_the_allocation(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.delete_work(summary)

    @then("I can not see the {summary} in the allocated list")
    def i_can_not_see_the_deleted_summary(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_allocated_work_in_available(summary)

    @when("I click on add new work button")
    def i_click_on_add_new_work_button(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.click_on_add_work_button()

    @when("I added new work with <summary>, <time> and saved")
    def i_click_on_add_new_work_button(context, summary, time):
        context.works_list = WorkListPage(context.driver)
        context.works_list.create_new_work_through_popup(summary, time)

    @then("I can see the success message {message} display on placeholder")
    def i_click_on_add_new_work_button(context, message):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_adding_work(message)

    @when("I click on the Message Menu from Works Menu")
    def i_click_on_message_menu_from_work(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_to_messages_menu()

    @when("I click on New message menu from Message Menus")
    def i_click_on_new_message_menu(context):
        context.works_list = WorkListPage(context.driver)
        context.works_list.navigate_to_new_message()

    @then("I should be able to added message summary {summary} in the message list")
    def i_can_see_added_message_summary_in_list(context, summary):
        context.works_list = WorkListPage(context.driver)
        context.works_list.verify_whether_added_message_summary(summary)
