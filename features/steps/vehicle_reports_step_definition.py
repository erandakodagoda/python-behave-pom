from behave import when, then

from pages.reports_page import ReportsPage


class VehicleReports:
    @when("I select Vehicles Menu from Reports Menus")
    def i_select_vehicles_menu_from_reports_menus(context):
        context.reportsPage = ReportsPage(context.driver)
        context.reportsPage.navigate_to_vehicle_reports_menu()

    @when("I select filters from reports")
    def i_select_filters_from_reports(context):
        for row in context.table:
            context.reportsPage.apply_filters(row['filterType'],
                                              row['reportType'],
                                              row['reportName'])

    @when('I select vehicles from left grid list')
    def i_select_vehicles_from_list(context):
        for row in context.table:
            context.reportsPage.select_vehicles(row['vehicleId'])

    @when('I select the date range {start_date} and {end_date}')
    def i_select_the_date_range(context, start_date, end_date):
        context.reportsPage.insert_start_date_and_end_date(start_date, end_date)

    @then("I click on Generate button")
    def i_click_on_generate_button(context):
        context.reportsPage.click_on_generate_button()

    @then("I should see report with vehicles")
    def i_should_see_report_with_vehicle_ids(context):
        for row in context.table:
            context.reportsPage.i_see_vehicle_in_the_report(row['reportVehicleId'])
