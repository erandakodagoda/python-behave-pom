from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_widget_page import WidgetManagementPage


class WidgetManagementStepDefinition:
    @when("I click on widget and then widget creator from manage menus")
    def i_click_on_widget_creator(context):
        context.menu_management = ManagePage(context.driver)
        context.menu_management.navigate_to_widgets_creator()

    @when("I added widget with details {vehicle}, {name}, {header} and saved")
    def i_added_widget_with_details(context, vehicle, name, header):
        context.widget_management = WidgetManagementPage(context.driver)
        context.widget_management.add_widget(name, header, vehicle)

    @then("I can see the added widget {name} is on the list")
    def i_can_see_the_added_widget(context, name):
        context.widget_management = WidgetManagementPage(context.driver)
        context.widget_management.verify_widget_in_list(name)

    @when("I click on widget {name} from the grid")
    def i_click_on_widget_from_grid(context, name):
        context.widget_management = WidgetManagementPage(context.driver)
        context.widget_management.click_on_widget_from_list(name)

    @then("I can not see the added widget {name} is on the list")
    def i_can_not_see_the_deleted_widget(context, name):
        context.widget_management = WidgetManagementPage(context.driver)
        context.widget_management.verify_widget_in_list(name)


