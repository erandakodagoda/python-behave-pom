from behave import when, then

from pages.manage_department_page import DepartmentManagementPage
from pages.manage_menu_page import ManagePage


class DepartmentManagementStepDefinition:
    @when("I enter department {name} and saved")
    def i_enter_department(context, name):
        context.manage_department = DepartmentManagementPage(context.driver)
        context.manage_department.create_department(name=name)

    @when("I click on department and then details from manage menus")
    def navigate_to_department_details(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_department_details()

    @then("I can see the added department {name} in the list")
    def i_can_see_added_department(context, name):
        context.manage_department = DepartmentManagementPage(context.driver)
        context.manage_department.verify_created_department(name)

    @then("I update alerts and added vehicle alerts to {contact} contact group")
    def i_updated_alerts_in_department(context, contact):
        context.manage_department = DepartmentManagementPage(context.driver)
        context.manage_department.update_department_details(contact)

    @when("I click on department {name} from list")
    def i_click_on_department_from_list(context, name):
        context.manage_department = DepartmentManagementPage(context.driver)
        context.manage_department.click_on_department_in_list(name)

    @then("I can not see the department {name} in the list")
    def i_can_not_see_the_department(context, name):
        context.manage_department = DepartmentManagementPage(context.driver)
        context.manage_department.verify_created_department(name)