from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_specialities_page import SpecialityManagementPage


class SpecialitiesManagementStepDefinition:
    @when("I click on specialities and then details from manage menus")
    def i_click_on_specialities_details(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_speciality_detail()

    @when("I added specialities with details {name} and saved")
    def i_added_specialities_with_name(context, name):
        context.specialities_magement = SpecialityManagementPage(context.driver)
        context.specialities_magement.add_specaility(name)

    @then("I can see the added speciality {name} in the grid")
    def i_can_see_the_added_speciality_name_in_grid(context, name):
        context.specialities_magement = SpecialityManagementPage(context.driver)
        context.specialities_magement.verify_speciality(name)

    @when("I click on the speciality {name} from grid")
    def i_click_on_the_speciality_from_grid(context, name):
        context.specialities_magement = SpecialityManagementPage(context.driver)
        context.specialities_magement.click_on_speciality_from_grid(name)

    @when("I click on edit button from the grid")
    def i_click_on_edit_button_from_grid(context):
        context.specialities_magement = SpecialityManagementPage(context.driver)
        context.specialities_magement.click_on_edit_button()

    @then("I can not see the deleted speciality {name} in the grid")
    def i_can_not_see_the_deleted_record(context, name):
        context.specialities_magement = SpecialityManagementPage(context.driver)
        context.specialities_magement.verify_speciality(name)
