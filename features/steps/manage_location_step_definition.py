from behave import when, then

from pages.location_page import LocationManagementPage
from pages.manage_menu_page import ManagePage


class ManageLocationStep:
    @when("I click on location and then type from manage menus")
    def i_click_and_navigate_to_location_type(context):
        context.manageMenu = ManagePage(driver=context.driver)
        context.manageMenu.navigate_to_location_types()

    @when("I click on location and then group from manage menus")
    def i_click_and_navigate_to_location_type(context):
        context.manageMenu = ManagePage(driver=context.driver)
        context.manageMenu.navigate_to_location_group()

    @when("I added location type {name} and saved")
    def i_added_location_type_and_saved(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.add_new_location_group(name=name)

    @then("I can see the new location type {name} in the list")
    def i_can_see_the_new_type_in_the_list(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_added_location_type(name=name)

    @when("I click on the location type {name} from the list")
    def i_click_on_the_location_type_from_list(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.click_on_the_location_type_in_list(name=name)

    @then("I can not see the new location type {name} in the list")
    def I_can_not_see_the_location_name(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_added_location_type(name=name)

    @when("I click on delete button")
    def i_click_on_delete_button(context):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.delete_location()

    @then("I can see location group {name} in the group list")
    def verify_location_group_in_list(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_new_location_group_in_list(name=name)

    @when("I select location group {group} from group list")
    def i_select_location_group_from_list(context, group):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.click_on_location_group(group)

    @when("I added location {location} from right panel")
    def i_select_location_group_from_list(context, location):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.assign_location_from_available(location)

    @then("I can see the assigned location {location} in assigned list")
    def i_can_see_the_assigned_location(context, location):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_location_assign_to_group(location)

    @when("I removed location {location} from right panel")
    def i_select_location_group_from_list(context, location):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.unassign_location_from_group(location)

    @then("I can see the un assigned location {location} in available list")
    def i_can_see_the_assigned_location(context, location):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_un_assigned_location_from_group(location)

    @when("I click on edit button")
    def i_click_on_edit_button(context):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.i_click_on_edit_button()

    @then("I can not see the deleted location {name} in the list")
    def i_can_not_see_deleted_location(context, name):
        context.locationManagement = LocationManagementPage(driver=context.driver)
        context.locationManagement.verify_assignment_remove(name)

