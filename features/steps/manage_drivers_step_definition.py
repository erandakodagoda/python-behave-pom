from behave import when, then

from pages.manage_drivers_page import ManageDriverPage
from pages.manage_menu_page import ManagePage


class ManageMenus:

    @when('I click on Drivers and then Driver Details from Manage Menus')
    def i_click_on_driver_details_from_manage(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_driver_details()

    @when("I click on Vehicles and then Vehicle Details from Manage Menus")
    def i_click_on_vehicle_details_from_manage(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_vehicle_details()

    @then('I should be able to see the Title of Menu Page {title}')
    def i_should_see_menu_page_title(context, title):
        context.manageMenus.verify_the_page_title(title)

    @when("I click on Driver menu and navigated to tags")
    def i_click_on_driver_menu_and_navigate_to_tags(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_drivers_tags()

    @when("I enter Driver RF Tag {tag} and saved")
    def i_enter_driver_rf_tag(context, tag):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.add_new_driver_tag(tag=tag)

    @then("I can see the added tag {tag} in the list")
    def i_can_see_the_added_tags(context, tag):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_the_tag_in_the_list(tag=tag)

    @when("I click on add button from driver")
    def i_click_on_add_button_from_driver(context):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_on_add_button()

    @when("I add a new driver with {short_name}, {first_name} and {last_name} and click on save")
    def i_added_a_new_driver(context, short_name, first_name, last_name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.add_new_driver(short_name=short_name, first_name=first_name, last_name=last_name)

    @then("I can see the {short_name} is getting displayed in the list")
    def i_can_see_the_driver_in_the_list(context, short_name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_added_driver_in_the_list(short_name=short_name)

    @then("I can not see the {short_name} is getting displayed in the list")
    def i_can_not_see_the_driver_in_the_list(context, short_name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_added_driver_in_the_list(short_name=short_name)

    @when("I click on the driver {short_name} from the list")
    def i_click_on_the_driver_from_list(context, short_name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_non_driver_from_list(short_name=short_name)

    @when("I updated driver detail and added {phone}")
    def i_update_drivers_phone_number(context, phone):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.update_phone_number_of_driver(phone=phone)

    @then("I can see the phone number {phone}")
    def i_can_see_the_phone_number(context, phone):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_number(phone=phone)

    @when("I add driver tag id {tag}")
    def i_added_driver_tag_id(context, tag):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.add_tag_to_driver(tag=tag)

    @when("I click on add new tag button")
    def i_click_on_add_new_tag(context):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_on_add_tag()

    @when("I click on drivers and navigated to driver groups")
    def i_click_on_drivers_navigated_to_group(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_drivers_group()

    @when("I click on drivers and navigated to driver permission")
    def i_click_on_drivers_navigated_to_permission(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_drivers_permission()

    @when("I click on drivers and navigated to driver assignment")
    def i_click_on_drivers_navigated_to_assignment(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_drivers_assignments()

    @then("I can see driver group {name} in the driver group list")
    def i_can_see_driver_group_in_the_list(context, name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_group_in_the_list(group=name)

    @then("I can not see driver group {name} in the driver group list")
    def i_can_not_see_driver_group_in_the_list(context, name):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_group_in_the_list(group=name)

    @when("I select driver group {group} from driver group list")
    def i_select_driver_group_from_list(context, group):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_on_driver_group(group=group)

    @when("I added driver {driver} from right panel")
    def i_added_driver_from_right(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.assign_driver_from_available(driver)

    @then("I can see the assigned driver {driver} in assigned driver list")
    def i_can_see_the_assigned_drivers(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_assign_to_group(driver=driver)

    @when("I removed driver {driver} from right panel")
    def i_remove_driver_from_assigned_list(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.unassign_driver_from_group(driver=driver)

    @then("I can see the unassigned driver {driver} in available list")
    def i_can_see_unassigned_driver_in_the_available_list(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_un_assigned_driver_from_group(driver=driver)

    @when("I click on drivers and then driver permissions from manage menus")
    def i_click_on_drivers_and_navigate_to_driver_permissions(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_drivers_permission()

    @when("I click on the driver name from the driver list {driver}")
    def i_click_on_the_driver_name_from_list(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_on_users_in_list(user=driver)

    @when("I assigned group {group} from available groups")
    def i_assigned_group_from_available(context, group):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.assign_driver_group_from_available(group=group)

    @then("I can see the assigned group {group} in the assigned groups")
    def i_can_see_the_assigned_group_in_the_list(context, group):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_group_assign_to_group(group=group)

    @when("I un assigned group {group} from assigned groups")
    def i_un_assigned_group_from_assigned_groups(context, group):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.unassign_driver_group_from_group(group=group)

    @then("I can see the un assigned group {group} in the available groups")
    def i_can_see_the_un_assigned_group_in_availabe_groups(context, group):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_un_assigned_driver_group_from_group(group=group)

    @when("I click on drivers and then manage assignment from manage menus")
    def i_click_on_drivers_and_navigate_to_driver_assignment(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_drivers_assignments()

    @when("I click on the vehicle {vehicle} and driver {driver} from list")
    def i_click_on_driver_and_vehicle_and_assigned(context, vehicle, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.assigned_driver_to_vehicle(driver=driver, vehicle=vehicle)

    @when("I click on add button in the center to assign")
    def i_click_on_add_button_to_assign(context):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.click_on_assign_driver_button()

    @then("I can see the assigned driver {driver} in the vehicle assignment list")
    def i_can_see_the_assigned_driver_in_vehicles(context, driver):
        context.manageDrivers = ManageDriverPage(context.driver)
        context.manageDrivers.verify_driver_assignment(driver=driver)