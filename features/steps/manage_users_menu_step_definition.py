from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_users_page import UserManagePage


class ManageUsersSteps:

    @when("I click on Users menu and navigated to roles and rights")
    def i_click_on_users_and_go_to_roles_and_rights(context):
        context.manageMenuPage = ManagePage(context.driver)
        context.manageMenuPage.click_on_user_menu_roles()

    @when("I click on add button to add roles")
    def i_click_on_add_button(context):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.click_on_add_role_button()

    @when("I added a new role with {role_name} and {all_vehicles}, {all_drivers}")
    def i_added_new_role_with_details(context, role_name, all_vehicles, all_drivers):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.add_user_role(role_name=role_name, all_vehicles=all_vehicles, all_drivers=all_drivers)

    @then("I can see added {role_name} in the list")
    def i_can_see_added_role_in_the_list(context, role_name):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_can_see_role_in_list(role_name=role_name)

    @then("I can not see the role {role_name} in the list")
    def i_can_not_see_added_role_in_the_list(context, role_name):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_can_see_role_in_list(role_name=role_name)

    @when("I click on {role_name} from roles list")
    def i_click_on_roles_from_roles_list(context, role_name):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_click_on_the_role_from_list(role_name=role_name)

    @then("I select {permission} from roles and rights")
    def i_click_on_all_the_roles(context, permission):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_change_the_permission_to_role(permission)
        context.userManagePage.i_click_on_save_changes()

    @when("I click on Users menu and navigated to details")
    def i_click_on_add_button(context):
        context.managePage = ManagePage(context.driver)
        context.managePage.i_click_on_user_detail_menu()

    @when("I click on add new user button")
    def i_click_add_new_user_button(context):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_click_on_add_user_button()

    @when("I added user with {username}, {fname}, {lname}, {password}, {phone}, {email}")
    def i_added_a_new_user_with(context, username, fname, lname, password, phone, email):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_add_user_with_details(username=username,
                                                       fname=fname,
                                                       lname=lname,
                                                       password=password,
                                                       phone=phone,
                                                       email=email)

    @then("I see the {username} in the list")
    def i_see_the_user_in_the_list(context, username):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_see_username_in_the_list(username=username)

    @when("I click on the username {username} from the user list")
    def i_click_on_the_user_nem_from_list(context, username):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.click_on_user_in_list(username=username)

    @then("I can not see the username {usernameo} in the list")
    def i_can_not_see_the_user_in_the_list(context, username):
        context.userManagePage = UserManagePage(context.driver)
        context.userManagePage.i_see_username_in_the_list(username=username)
