from behave import then, when

from pages.dashboard import DashboardPage


class DashboardSteps:

    @then("I verify dashboard page appearing with the {username}")
    def i_get_navigated_to_home_page(context, username):
        context.dashboardPage = DashboardPage(context.driver)
        name = "Welcome " + username
        context.dashboardPage.i_validate_the_dashboard_username(name)

    @then("I should be able to see the {group} under the vehicle tab")
    def i_should_see_group_under_the_vehicle(context, group):
        context.dashboardPage.navigate_to_vehicles_tab()
        context.dashboardPage.verify_vehicle_group(group)

    @then("I click on vehicle from the list {vehicle}")
    def i_click_on_vehicle_from_the_list(context, vehicle):
        context.dashboardPage.verify_vehicle_group(vehicle)

    @then("I should be able to see the {run} under the driver tab")
    def i_should_see_group_under_the_vehicle(context, run):
        context.dashboardPage.navigate_to_driver_tab()
        context.dashboardPage.verify_driver_run(run)

    @then("I should be able to see the vehicle details")
    def i_should_be_able_to_see_the_vehicle_details(context):
        for row in context.table:
            context.dashboardPage.verify_vehicle_info(row['vehicleName'],
                                                      row['currentDriver'],
                                                      row['assignedDriver'])

    @then("I logged out from the application")
    def i_logged_out_from_application(context):
        context.dashboardPage.logout_from_application()

    @then("I should be able to see the {run} under the run tab")
    def i_should_be_able_to_see_run_tab(context, run):
        context.dashboardPage.navigate_to_run_tab()
        context.dashboardPage.verify_run(run)

    @then("I click on run group from the list {runId}")
    def i_click_on_run_group_from_list(context, runId):
        context.dashboardPage.verify_run_in_runs(runId)

    @then("I should be able to see the {location} under the location tab")
    def i_should_be_able_to_see_the_location(context, location):
        context.dashboardPage.navigate_to_location_tab()
        context.dashboardPage.verify_location_in_locations(location)

    @then("I click on location from the list {location}")
    def i_click_on_location_from_list(context, location):
        context.dashboardPage.click_on_location_from_list(location)

    @then("I should be able to see the location details")
    def i_should_see_vehicle_details(context):
        for row in context.table:
            context.dashboardPage.verify_location_info(row['locationName'],
                                                       row['locationGroup'],
                                                       row['locationType'],
                                                       row['locationDepart'],
                                                       row['isPending'])

    @then("I should be able to see the {alert} under the alert tab")
    def i_should_be_able_to_see_alert(context, alert):
        context.dashboardPage.navigate_to_alert_tab()
        context.dashboardPage.verify_alert_in_alerts(alert)

    @then("I click on alert from the list {car_id}")
    def i_click_on_alert_from_list(context, car_id):
        context.dashboardPage.verify_alert_in_alerts(car_id)

    @then("I should be able to see the alert details")
    def i_should_be_able_to_see_alert(context):
        for row in context.table:
            context.dashboardPage.verify_alert_info(row['alertMessage'],
                                                    row['alertVehicle'],
                                                    row['alertFleetId'])

    @then("I click on run from the list {run_id}")
    def i_click_on_run_from_list(context, run_id):
        context.dashboardPage.verify_driver_run(run_id)

    @then("I should be able to see the {alarm} under the alarm tab")
    def i_should_see_alarms_under_alarm_tab(context, alarm):
        context.dashboardPage.navigate_to_alarm_tab()
        context.dashboardPage.verify_alarm_in_alarms(alarm)

    @then("I click on alarm from the list {car_id}")
    def i_click_on_alarm_from_list(context, car_id):
        context.dashboardPage.verify_alarm_in_alarms(car_id)

    @when("I click on Reports Menu from main Menu")
    def i_click_on_reports_menu(context):
        context.dashboardPage = DashboardPage(context.driver)
        context.dashboardPage.navigate_to_reports_menu()

    @when("I click on Manage Menu from main menu")
    def i_click_on_manage_menu(context):
        context.dashboardPage = DashboardPage(context.driver)
        context.dashboardPage.click_on_manage_menu()

    @when("I click on status menu")
    def i_click_on_status_menu(context):
        context.dashboardPage = DashboardPage(context.driver)
        context.dashboardPage.click_on_status_menu()

    @when("I click on Works Menu from main menu")
    def i_click_on_works_menu_from_main_menu(context):
        context.dashboardPage = DashboardPage(context.driver)
        context.dashboardPage.navigate_to_works_menu()

    @when("I click on Dashboard Menu From Main Menu")
    def i_click_on_dashboard_menu(context):
        context.dashboardPage = DashboardPage(context.driver)
        context.dashboardPage.navigate_to_dashboard_menu()







