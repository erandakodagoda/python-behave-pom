from behave import when, then
from pages.manage_menu_page import ManagePage
from pages.manage_runs_page import ManageRunPage


class ManageRunStep:
    @when("I click on run and then details from manage menus")
    def i_click_on_run_and_navigate_to_details(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_ro_runs_details()

    @when("I click on add button from run")
    def i_click_n_add_button_from_run(context):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.click_on_add_button()

    @when("I add a run with {name}")
    def i_added_new_run(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.add_new_run(run=name)

    @then("I can see the added run in the {name} list")
    def i_can_see_the_added_run_in_the_list(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_added_run_in_list(run=name)

    @then("I can not see the deleted run in the {name} list")
    def i_can_not_see_the_deleted_run_in_the_list(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_added_run_in_list(run=name)

    @when("I click on the run {name} from run list")
    def i_clisk_on_run_in_the_list(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.click_on_run_from_list(run=name)

    @when("I click on run and then groups from manage menus")
    def i_click_on_run_and_navigate_to_groups(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_runs_groups()

    @then("I can see driver group {name} in the run group list")
    def i_can_see_the_added_run_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_added_run_group(group=name)

    @then("I can not see driver group {name} in the run group list")
    def i_can_not_see_the_run_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_added_run_group(group=name)

    @when("I add a run group with {name} and saved")
    def i_add_run_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.add_new_run_group(group=name)

    @when("I select run group {group} from run group list")
    def i_select_run_from_run_group(context, group):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.click_on_run_group(run=group)

    @when("I added run {run} from right panel")
    def i_added_run_from_available_list(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.assign_run_from_available(run=run)

    @then("I can see the assigned run {run} in assigned run list")
    def i_can_see_the_assigned_run(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_run_assign_to_group(run=run)

    @when("I unassigned run {run} from left panel")
    def i_added_run_from_available_list(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.unassign_run_from_group(run=run)

    @then("I can see the un assigned run {run} in available run list")
    def i_can_see_the_assigned_run(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_un_assigned_run_from_group(run=run)

    @when("I click on run and then permission from manage menus")
    def i_click_on_run_and_then_permission_from_manage_menus(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_runs_permissions()

    @when("I click on driver name {driver} from the run list")
    def i_click_on_driver_from_the_driver_list(context, driver):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.click_on_driver_from_list(driver=driver)

    @when("I assigned a run permission group with {name} and saved")
    def i_assigned_run_permission_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.assign_run_permission_from_available(run=name)

    @then("I can see assigned run group {name} in the run group list")
    def i_verified_assigned_run_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_run_permission_assign_to_group(run=name)

    @when("I un assigned a run permission group with {name} and saved")
    def i_un_assigned_run_permission(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.unassign_run_permission_from_group(run=name)

    @then("I can see un assigned run group {name} in the run group list")
    def i_can_see_unassigned_run_group(context, name):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_un_assigned_run_permission_from_group(run=name)

    @when("I click on run and then manage assignment from manage menus")
    def i_navigate_to_run_assignment(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_runs_assignments()

    @when("I click on the vehicle {vehicle} and run {run} from list")
    def i_click_on_the_vehicle_and_run_to_assign(context, run, vehicle):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.assigned_run_to_vehicle(vehicle=vehicle, run=run)

    @then("I can see the assigned run {run} in the vehicle assignment list")
    def i_can_see_the_assignment_in_vehicle(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_run_assignment(run=run)

    @when("I click on the close button in vehicle run {run} from list")
    def i_click_on_close_button_from_vehicles(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.remove_run_assignment(run=run)

    @then("I can see the assigned run {run} in the vehicle has been removed")
    def i_can_see_the_assigned_run_is_removed(context, run):
        context.manageRuns = ManageRunPage(context.driver)
        context.manageRuns.verify_assignment_remove(run=run)




