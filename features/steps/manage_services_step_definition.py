from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_services_page import ServiceManagementPage


class ServiceManagementStepDefinition:
    @when("I click on services and then details from manage menus")
    def i_click_on_service_details(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_service_details()

    @when("I click on services and then schedule from manage menus")
    def i_click_on_service_schedule(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_service_schedules()

    @when("I click on vehicle {vehicle} from services list")
    def i_click_on_vehicle_from_service_list(context, vehicle):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.select_vehicle_from_vehicle_list(vehicle=vehicle)

    @when("I added the service record with {priority}, {cost}, {date} and saved")
    def i_added_service_with_details(context, priority, cost, date):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.add_new_service_record(priority, cost, date)

    @then("I should be able to see the service record {date} in the history table")
    def i_should_see_the_added_service(context, date):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.verify_service_on_grid(date)

    @when("I click on service record date {date} from list")
    def i_click_on_service_record_date(context, date):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.click_on_service_record(date)

    @when("I changed the service record date {date_new} and saved")
    def i_changed_the_service_record_date(context, date_new):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.edit_service_record_and_save(date_new)

    @when("I click on delete button from the list")
    def i_click_on_delete_button_from_grid(context):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.delete_service_button()

    @then("I should not be able to see the service record {date} in the history table")
    def i_should_not_not_see_the_service_record(context, date):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.verify_service_on_grid(date)

    @when("I changed the starting odometer {odo} and saved")
    def i_changed_the_starting_odometer(context, odo):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.add_start_odometer_and_save(odo)

    @then("I can see the added starting odometer {odo} record in field")
    def i_can_see_the_added_odometer_record(context, odo):
        context.service_management = ServiceManagementPage(context.driver)
        context.service_management.verify_changed_odometer_record(odo)

