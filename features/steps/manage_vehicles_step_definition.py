from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_vehicle_page import VehicleManagePage


class ManageVehiclesSteps:

    @when("I click on Vehicles menu and navigated to diagnostic templates")
    def i_click_on_vehicles_diagnoctic_template_menu(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_vehicle_diagnostic_temp()

    @when("I click on add new template button")
    def i_click_on_add_new_template(context):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_add_new_template()

    @when("I enter template name {template} and save")
    def i_enter_template_name_and_click_save(context, template):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_new_template(template_name=template)

    @then("I should be able to see the added template in the list {template}")
    def i_should_see_template_in_list(context, template):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_the_template_in_the_list(template_name=template)

    @then("I can not see the diagnostic template {name} in the list")
    def i_should_not_see_template_in_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_the_template_in_the_list(template_name=name)

    @when("I added an alert entry to the diagnostic template")
    def i_added_an_alert_entry(context):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_add_new_template()

    @when("I click on the template {name} from the list")
    def i_click_on_the_template_from_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_template_from_list(template_name=name)

    @when("I click on Vehicles menu and navigated to classes")
    def i_click_on_vehicles_diagnoctic_template_menu(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_vehicle_classes()

    @when("I added new vehicle class with {name}")
    def i_added_new_vehicle_class(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_vehicle_class(class_name=name)

    @then("I can see {name} in the class list")
    def i_can_see_class_in_the_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_class_in_list(class_name=name)

    @then("I can not see {name} in the class list")
    def i_can_not_see_class_in_the_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_class_in_list(class_name=name)

    @when("I click on vehicle class {name} from the list")
    def click_on_vehicle_class_from_the_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_vehicle_class_from_the_list(vehicle=name)



    @when("I click on Vehicles menu and navigated to operating hours")
    def i_click_on_vehicles_and_navigate_to_operation_hours(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigated_to_operating_hours()

    @when("I click on add operating hours button")
    def i_click_on_add_operating_hours(context):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_add_new_template()

    @when("I click on add new class button")
    def i_click_on_add_class_button(context):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_add_new_template()

    @when("I add operating hours entry with {name}")
    def i_added_operating_hours(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_new_operating_hours(template_name=name)

    @then("I can see the added {name} in the grid")
    def i_can_see_the_record_in_the_grid(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_the_record_visibility(name)

    @then("I can not see the added {name} in the grid")
    def i_can_see_the_record_in_the_grid(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_the_record_visibility(name)

    @when("I click on the operating hours {name} in the grid")
    def i_can_see_the_record_in_the_grid(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_operating_record(name)

    @when("I click on Vehicles menu and navigated to permission")
    def i_click_on_vehicles_and_navigate_to_permission(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_vehicle_permission()

    @when("I select {driver} from the list")
    def i_select_driver_from_list(context, driver):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_the_driver_from_list(driver_name=driver)

    @when("I added group {group} from right panel")
    def i_added_group_from_right_panel(context, group):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_group_to_driver(group_name=group)

    @then("I can see the {group} in the left panel")
    def i_can_see_the_group_in_the_list(context, group):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_group_assign_to_driver(group_name=group)

    @when("I removed group {group} from left panel")
    def i_removed_group_from_panel(context, group):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.unassign_group_from_drivers(group_name=group)

    @then("I can see the {group} in the right panel")
    def i_can_see_the_group_in_right_panel(context, group):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_un_assigned_group_to_driver(group_name=group)

    @when("I click on Vehicles menu and navigated to groups")
    def i_navigated_to_vehicle_groups(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_vehicle_groups()

    @when("I click on add group button")
    def i_click_on_add_group_button(context):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_add_group_button()

    @when("I enter group {name} and saved")
    def i_enter_group_and_saved(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_new_group(group_name=name)

    @then("I can see group {name} in the group list")
    def i_can_see_group_in_the_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_added_group_in_the_list(group_name=name)

    @then("I can not see group {name} in the group list")
    def i_can_see_group_in_the_list(context, name):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_added_group_in_the_list(group_name=name)

    @when("I added vehicle {vehicle} from right panel")
    def i_added_vehicle_from_right_panel(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_vehicles_to_group(vehicle=vehicle)

    @then("I can see the assigned vehicle {vehicle} in assigned list")
    def i_can_see_added_vehicle_in_the_left_panel(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_vehicle_assign_to_group(vehicle=vehicle)

    @when("I select group {group} from group list")
    def i_select_group_from_list(context, group):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_the_group_from_list(group_name=group)

    @when("I removed vehicle {vehicle} from right panel")
    def i_removed_vehicle_from_right_panel(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.unassign_vehicle_from_group(vehicle=vehicle)

    @then("I can see the unassigned vehicle {vehicle} in available list")
    def i_can_see_removed_vehicle_in_the_right_panel(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_un_assigned_vehicle_from_group(vehicle=vehicle)

    @when("I click on Vehicles menu and navigated to details")
    def i_navigate_to_vehicle_details(context):
        context.manageMenus = ManagePage(context.driver)
        context.manageMenus.navigate_to_vehicle_details()

    @when("I click on vehicle {vehicle} from the vehicle list")
    def i_click_on_vehicle_from_vehicle_list(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.click_on_vehicle_from_vehicle_list(vehicle=vehicle)

    @then("I verify vehicle {vehicle} appears in the text field")
    def i_verify_vehicle_on_the_text_field(context, vehicle):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_name_in_the_text_field(vehicle=vehicle)

    @when("I edit the vehicle id {id} to selected vehicle and saved")
    def i_edit_the_vehicle_id(context, id):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.add_vehicle_id_to_the_text_field(id)

    @then("I verify vehicle id {vehicle_id} on field")
    def i_verify_vehicle_and_id_on_fields(context, vehicle_id):
        context.manageVehicles = VehicleManagePage(context.driver)
        context.manageVehicles.verify_name_and_id(vehicle_id=vehicle_id)




