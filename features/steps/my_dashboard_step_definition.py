from behave import when, then

from pages.my_dashboard_page import MyDashboardPage


class MyDashboardStepDefinition:
    @when("I add widget {widget} to  my dashboard")
    def i_added_widget_to_my_dashboard(context, widget):
        context.my_dashboard = MyDashboardPage(context.driver)
        context.my_dashboard.add_widget(widget)

    @then("I can see the added {widget} in my dashboard")
    def i_can_see_the_added_widget_in_my_dashboard(context, widget):
        context.my_dashboard = MyDashboardPage(context.driver)
        context.my_dashboard.verify_added_widget(widget)

    @when("I click on delete button of the widget {widget}")
    def i_click_on_delete_button_of_the_widget(context, widget):
        context.my_dashboard = MyDashboardPage(context.driver)
        context.my_dashboard.delete_widget(widget)

    @then("I can not see the deleted {widget} in my dashboard")
    def i_can_not_see_the_deleted_widget_in_my_dashboard(context, widget):
        context.my_dashboard = MyDashboardPage(context.driver)
        context.my_dashboard.verify_added_widget(widget)