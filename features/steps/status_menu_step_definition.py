from behave import when, then

from pages.status_menu_page import StatusPage


class StatusMenuSteps:
    @when("I click on grouped menu")
    def i_clicked_on_grouped_menu(context):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_click_on_grouped_menu()

    @when("I can see group {group_name} in the table")
    def i_can_see_group_names(context, group_name):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_see_group_name_in_the_data(group_name)

    @then("I can see alert {alert} in the table")
    def i_can_see_alerts(context, alert):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_see_group_name_in_the_data(alert)

    @when("I click on ungrouped menu")
    def i_clicked_on_ungrouped_menu(context):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_click_on_ungrouped()

    @when("I click on services menu")
    def i_click_on_services_menu(context):
        context.statusPage= StatusPage(context.driver)
        context.statusPage.i_click_on_services_menu()

    @then("I can see vehicle {vehicle} in the table")
    def i_can_see_the_vehicle(context, vehicle):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_click_on_vehicle_from_list(vehicle)

    @then("I change the operational status to {status}")
    def i_change_the_operational_status(context, status):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_change_the_operational_status(status)

    @then("I click on the details on vehicle {vehicle}")
    def i_click_on_the_details_on_vehicle(context, vehicle):
        context.statusPage = StatusPage(context.driver)
        context.statusPage.i_click_on_details_page(vehicle)


