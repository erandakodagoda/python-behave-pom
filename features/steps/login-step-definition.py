from behave import *

from pages.login_page import LoginPage
from config.config import Config


class LoginSteps:

    @given("I am on the AVM Login Page in")
    def i_am_on_the_login_page(context):
        for row in context.table:
            context.environment = Config.get_environment(row['environment'])
            context.driver.get(Config.get_environment(row['environment']))
            context.loginPage = LoginPage(context.driver)

    @when("I navigate to AVM login page")
    def i_navigated_to_login_page(context):
        context.loginPage.get_login_page_header_image()

    @when("I enter username and password in")
    def i_enter_user_and_password(context):
        for row in context.table:
            context.loginPage.enter_username_and_password(row['username'], row['password'])
            return

    @when("I click on login button")
    def i_click_login_button(context):
        context.loginPage.click_on_login_button()

    @then("I verify validation message {validation}")
    def i_get_validation_message(context, validation):
        context.loginPage.verified_invalid_user_pass(validation)

    @when("I login to the application using username and password in")
    def i_login_using_username_and_password(context):
        context.loginPage.get_login_page_header_image()
        for row in context.table:
            context.loginPage.enter_username_and_password(row['username'], row['password'])
            context.loginPage.click_on_login_button()

