from behave import when, then

from pages.manage_menu_page import ManagePage
from pages.manage_tools_page import ToolsManagementPage


class ToolsManagementStepDefinition:
    @when("I click on tools and then fuel consumption from manage menus")
    def i_click_on_fuel_consumption_from_menu(context):
        context.manage_menus = ManagePage(context.driver)
        context.manage_menus.navigate_to_fuel_consumption()

    @when("I click on vehicle with {name} in vehicle fuel list")
    def i_click_on_vehicle_from_list(context, name):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.click_on_vehicle(name)

    @when("I added fuel consumption entry with {on_date} and saved")
    def i_added_fuel_consumption_entry(context, on_date):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.save_fuel_consumption_record(on_date)

    @then("I can see the added fuel consumption record {on_date}")
    def i_can_see_added_fuel_record(context, on_date):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.verify_added_consumption_record(on_date)

    @when("I changed the fuel consumption record date from {on_date} to {new_date}")
    def i_edit_the_fuel_date_and_saved(context, on_date, new_date):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.edit_consumption_record(on_date, new_date)

    @when("I delete the fuel consumption record from grid")
    def i_delete_the_fuel_consumption_record(context):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.delete_consumption_record()

    @then("I can not see the added fuel consumption record {on_date}")
    def i_can_not_see_the_deleted_consumption_record(context, on_date):
        context.tools_management = ToolsManagementPage(context.driver)
        context.tools_management.verify_added_consumption_record(on_date)