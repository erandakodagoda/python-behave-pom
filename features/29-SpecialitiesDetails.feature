Feature: As a registered user I should be able to add/edit/delete specialities

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add specialities
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on specialities and then details from manage menus
    And I click on add group button
    And I added specialities with details <name> and saved
    Then I can see the added speciality <name> in the grid

    Examples:
      | username           | name      |
      | virendra@ausgridft | Demo Test |

  Scenario Outline: As a registered user I should be able to edit specialities
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on specialities and then details from manage menus
    And I click on the speciality <name> from grid
    And I click on edit button from the grid
    And I added specialities with details <newName> and saved
    Then I can see the added speciality <newName> in the grid

    Examples:
      | username           | name      | newName       |
      | virendra@ausgridft | Demo Test | Demo New Test |

  Scenario Outline: As a registered user I should be able to delete specialities
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on specialities and then details from manage menus
    And I click on the speciality <name> from grid
    And I click on delete button
    Then I can not see the deleted speciality <name> in the grid

    Examples:
      | username           | name          |
      | virendra@ausgridft | Demo New Test |