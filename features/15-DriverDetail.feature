Feature: As a registered user I should be able to add new Drivers and edit existing drivers

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

#  Scenario Outline: As a registered user I should be able to add new drivers
#    When I login to the application using username and password in:
#      | username           | password  |
#      | virendra@ausgridft | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    When I click on Manage Menu from main menu
#    And I click on Drivers and then Driver Details from Manage Menus
#    And I click on add button from driver
#    And I add a new driver with <shortName>, <firstName> and <lastName> and click on save
#    Then I can see the <shortName> is getting displayed in the list
#
#    Examples:
#      | username           | shortName   | firstName | lastName |
#      | virendra@ausgridft | Demo@Driver | Demo      | Driver   |
#
#
#  Scenario Outline: As a registered user I should be able to edit drivers details
#    When I login to the application using username and password in:
#      | username           | password  |
#      | virendra@ausgridft | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    When I click on Manage Menu from main menu
#    And I click on Drivers and then Driver Details from Manage Menus
#    And I click on the driver <shortName> from the list
#    And I updated driver detail and added <phoneNumber>
#    And I click on the driver <shortName> from the list
#    Then I can see the phone number <phoneNumber>
#
#    Examples:
#      | username           | shortName   | phoneNumber   |
#      | virendra@ausgridft | Demo@Driver | 6784347394839 |
#
#  Scenario Outline: As registered user I should be able to assign tags to drivers
#    When I login to the application using username and password in:
#      | username           | password  |
#      | virendra@ausgridft | Pinpoint1 |
#    Then I verify dashboard page appearing with the <username>
#    When I click on Manage Menu from main menu
#    And I click on Drivers and then Driver Details from Manage Menus
#    And I click on the driver <shortName> from the list
#    And I add driver tag id <tag>
#
#    Examples:
#      | username           | shortName   | tag           |
#      | virendra@ausgridft | Demo@Driver | 6784347394839 |

  Scenario Outline: As a registered user I should be able to delete drivers
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Drivers and then Driver Details from Manage Menus
    And I click on the driver <shortName> from the list
    And I click on delete button
    Then I can not see the <shortName> is getting displayed in the list

    Examples:
      | username           | shortName   |
      | virendra@ausgridft | Demo@Driver |