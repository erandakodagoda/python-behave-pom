Feature: As a registered user I should be able to add/edit/delete services

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add service records to vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on services and then details from manage menus
    And I click on vehicle <vehicle> from services list
    And I click on add group button
    And I added the service record with <priority>, <cost>, <date> and saved
    Then I should be able to see the service record <date> in the history table

    Examples:
      | vehicle       | priority | cost | date                |
      | Rodney Subaru | Normal   | 100  | 22/03/2021 11:40 AM |

  Scenario Outline: As a registered user I should be able to edit service records in vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on services and then details from manage menus
    And I click on vehicle <vehicle> from services list
    And I click on service record date <date> from list
    And I changed the service record date <dateNew> and saved
    Then I should be able to see the service record <dateNew> in the history table

    Examples:
      | vehicle       | dateNew             | date                |
      | Rodney Subaru | 23/03/2021 11:40 AM | 22/03/2021 11:40 AM |

  Scenario Outline: As a registered user I should be able to delete service records in vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on services and then details from manage menus
    And I click on vehicle <vehicle> from services list
    And I click on service record date <date> from list
    And I click on delete button from the list
    Then I should not be able to see the service record <dateNew> in the history table

    Examples:
      | vehicle       | date                |
      | Rodney Subaru | 23/03/2021 11:40 AM |