Feature: As a registered user I should be able to create/edit/delete widgets

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add widget
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on widget and then widget creator from manage menus
    And I click on add group button
    And I added widget with details <vehicle>, <name>, <header> and saved
    Then I can see the added widget <name> is on the list

    Examples:
      | username           | vehicle   | name        | header      |
      | virendra@ausgridft | WifiTest5 | Demo Widget | Demo Widget |

  Scenario Outline: As a registered user I should be able to edit widget
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on widget and then widget creator from manage menus
    And I click on widget <name> from the grid
    And I click on edit button from the grid
    And I added widget with details <vehicle>, <name2>, <header> and saved
    Then I can see the added widget <name2> is on the list

    Examples:
      | username           | vehicle   | name          | header      | name2       |
      | virendra@ausgridft | WifiTest5 | Eranda Driver | Demo Widget | Demo Widget |

  Scenario Outline: As a registered user I should be able to delete widget
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on widget and then widget creator from manage menus
    And I click on widget <name> from the grid
    And I click on delete button
    Then I can not see the added widget <name> is on the list

    Examples:
      | username           | name        |
      | virendra@ausgridft | Demo Widget |