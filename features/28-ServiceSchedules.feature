Feature: As a registered user I should be able to edit the service schedule

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user i should be able to edit the service schedule
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on services and then schedule from manage menus
    And I click on vehicle <vehicle> from services list
    And I changed the starting odometer <odo> and saved
    Then I can see the added starting odometer <odo> record in field

    Examples:
      | username           | vehicle       | odo  |
      | virendra@ausgridft | Rodney Subaru | 1000 |