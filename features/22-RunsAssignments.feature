Feature: As  a registered user i should be able to manage the assignments of vehicles towards runs

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to assign runs to vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then manage assignment from manage menus
    And I click on the vehicle <vehicle> and run <run> from list
    And I click on add button in the center to assign
    Then I can see the assigned run <run> in the vehicle assignment list

    Examples:
      | username           | run           | vehicle   |
      | virendra@ausgridft | ErandaTestRun | WifiTest5 |

  Scenario Outline: As a registered user I should be able to remove assign runs to vehicles
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on run and then manage assignment from manage menus
    And I click on the close button in vehicle run <run> from list
    Then I can see the assigned run <run> in the vehicle has been removed

    Examples:
      | username           | run           |
      | virendra@ausgridft | ErandaTestRun |