Feature: As a registered user I should be able to verify the status menu

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to navigate the statuses and verify grouped alerts
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    When I click on status menu
    And I click on grouped menu
    And I can see group <groupName> in the table
    Then I can see alert <alert> in the table

    Examples:
      | groupName | alert |
      | Ungrouped | 10056 |

  Scenario Outline: As a registered user I should be able to navigate the statuses and verify ungrouped alerts
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    When I click on status menu
    And I click on ungrouped menu
    Then I can see alert <alert> in the table

    Examples:
      | alert |
      | 10057 |

  Scenario Outline: As a registered user I should be able to navigate the services and change the operational status
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    When I click on status menu
    And I click on services menu
    Then I can see vehicle <vehicle> in the table
    And I change the operational status to <status>

    Examples:
      | vehicle | status |
      | 61517   | Yes    |

  Scenario Outline: As a registered user I should be able to navigate to Service Details Page by Clicking on details
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    When I click on status menu
    And I click on services menu
    Then I can see vehicle <vehicle> in the table
    And I click on the details on vehicle <vehicle>

    Examples:
      | vehicle |
      | 61517   |