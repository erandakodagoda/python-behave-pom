Feature: As a registered user I should be able to add new groups and assign drivers to groups

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new driver group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and navigated to driver groups
    And I click on add group button
    And I enter group <name> and saved
    Then I can see driver group <name> in the driver group list

    Examples:
      | username           | name              |
      | virendra@ausgridft | Demo Driver Group |

  Scenario Outline: As a registered user I should be able assign drivers to group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and navigated to driver groups
    And I select driver group <group> from driver group list
    And I added driver <driver> from right panel
    Then I can see the assigned driver <driver> in assigned driver list

    Examples:
      | username           | driver      | group             |
      | virendra@ausgridft | Demo@Driver | Demo Driver Group |

  Scenario Outline: As a registered user I should be able to un assign driver from group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and navigated to driver groups
    And I select driver group <group> from driver group list
    And I removed driver <driver> from right panel
    Then I can see the unassigned driver <driver> in available list

    Examples:
      | username           | driver      | group             |
      | virendra@ausgridft | Demo@Driver | Demo Driver Group |

  Scenario Outline: As a registered user I should be able to edit driver group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and navigated to driver groups
    And I select driver group <name> from driver group list
    And I click on edit button
    And I enter group <name2> and saved
    Then I can see driver group <name2> in the driver group list

    Examples:
      | username           | name              | name2      |
      | virendra@ausgridft | Demo Driver Group | Demo Group |

  Scenario Outline: As a registered user I should be able to delete driver group
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on drivers and navigated to driver groups
    And I select driver group <name> from driver group list
    And I click on delete button
    Then I can not see driver group <name> in the driver group list

    Examples:
      | username           | name       |
      | virendra@ausgridft | Demo Group |