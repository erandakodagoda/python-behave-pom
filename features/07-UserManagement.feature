Feature: As a registered user I should be able to add edit users from Manage Users

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add new users
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to details
    And I click on add new user button
    And I added user with <usernamed>, <fname>, <lname>, <password>, <phone>, <email>
    Then I see the <usernamed> in the list

    Examples:
      | username           | usernamed      | fname  | lname    | password | phone        | email               |
      | virendra@ausgridft | Eranda@Drivers | Eranda | Kodagoda | Test@123 | +94763040064 | eranda.k@eyepax.com |

  Scenario Outline: As a registered user I should be able to edit existing users
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to details
    And I click on the username <usernameo> from the user list
    And I added user with <usernamen>, <fname>, <lname>, <password>, <phone>, <email>
    Then I see the <usernamen> in the list

    Examples:
      | username           | usernameo      | fname  | lname    | password | phone        | email               | usernamen    |
      | virendra@ausgridft | Eranda@Drivers | Eranda | Kodagoda | Test@123 | +94763040064 | eranda.k@eyepax.com | Test@DriverN |

  Scenario Outline: As a registered user I should be able to delete users
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Users menu and navigated to details
    And I click on the username <usernameo> from the user list
    And I click on delete button
    Then I can not see the username <usernameo> in the list

    Examples:
      | username           | usernameo    |
      | virendra@ausgridft | Test@DriverN |