Feature: As a registered user I should be able to create Diagnostic templates under vehicles

  Background:
    Given I am on the AVM Login Page in:
      | environment |
      | uat         |

  Scenario Outline: As a registered user I should be able to add diagnostic templates
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to diagnostic templates
    And I click on add new template button
    And I enter template name <name> and save
    Then I should be able to see the added template in the list <name>

    Examples:
      | username           | name          |
      | virendra@ausgridft | Demo Template |

  Scenario Outline: As a registered user I should be able to set alerts for diagnostic templates
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to diagnostic templates
    And I click on the template <name> from the list
    And I added an alert entry to the diagnostic template

    Examples:
      | username           | name          |
      | virendra@ausgridft | Demo Template |

  Scenario Outline: As a registered user I should be able delete the diagnostic templates
    When I login to the application using username and password in:
      | username           | password  |
      | virendra@ausgridft | Pinpoint1 |
    Then I verify dashboard page appearing with the <username>
    When I click on Manage Menu from main menu
    And I click on Vehicles menu and navigated to diagnostic templates
    And I click on the template <name> from the list
    And I click on delete button
    Then I can not see the diagnostic template <name> in the list

    Examples:
      | username           | name          |
      | virendra@ausgridft | Demo Template |
