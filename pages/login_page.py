from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class LoginPage(BasePage):
    login_page_header_image = (By.XPATH, "//img[@id='logo']")
    username_text_field = (By.ID, "UserName")
    password_text_field = (By.ID, "Password")
    login_button = (By.ID, "btnLogin")
    validation_message = (By.XPATH, "//*[@id='frmFormLogin'] //ul/li")

    def __init__(context, driver):
        super().__init__(driver)

    def get_login_page_header_image(context):
        return context.verifyElementVisible(context.login_page_header_image)

    def enter_username_and_password(context, username, password):
        context.sendKeysToElement(context.username_text_field, username)
        context.clickOnElement(context.password_text_field)
        context.sendKeysToElement(context.password_text_field, password)

    def click_on_login_button(context):
        context.clickOnElement(context.login_button)

    def get_page_title(context, title):
        context.getTitle(title)

    def verified_invalid_user_pass(context, validation):
        text = context.getTextOfElement(context.validation_message)
        assert text == validation


