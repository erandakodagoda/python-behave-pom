import re
import time

from selenium.common.exceptions import StaleElementReferenceException, ElementNotInteractableException, \
    ElementNotVisibleException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ExpectedConditions
import logging
from config import screenCapture, LocatorTypeUnidentifiedException

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


def find_word(word):
    return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search



class BasePage:
    def __init__(context, driver):
        logging.info("Driver is initializing...")
        context.driver = driver

    def clickOnElement(context, by_locator):
        logging.info("Waiting until the element to be clickable")
        try:
            WebDriverWait(context.driver, 10).until(ExpectedConditions.element_to_be_clickable(by_locator)).click()
        except Exception as e:
            screenCapture.capture_screenshot(context=context, name="Click Element")
            logging.error(e)

    def sendKeysToElement(context, by_locator, text):
        logging.info("Waiting to send keys to the element")
        try:
            WebDriverWait(context.driver, 10).until(ExpectedConditions.presence_of_element_located(by_locator)).clear()
            WebDriverWait(context.driver, 10).until(ExpectedConditions.presence_of_element_located(by_locator)).send_keys(
                text)
        except Exception as e:
            screenCapture.capture_screenshot(context=context, name="Text Entered")
            logging.error(e)

    def getTextOfElement(context, by_locator):
        logging.info("Waiting until the element to be visible to get text")
        try:
            text = WebDriverWait(context.driver, 10).until(
                ExpectedConditions.visibility_of_element_located(by_locator)).text
            return text
        except Exception as e:
            screenCapture.capture_screenshot(context=context, name="Get Text")
            logging.error(e)

    def verifyElementVisible(context, by_locator):
        logging.info("Verifying the visibility of the element")
        try:
            value = WebDriverWait(context.driver, 10).until(
                ExpectedConditions.visibility_of_element_located(by_locator))
        except Exception as e:
            screenCapture.capture_screenshot(context=context, name="Element Visible")
            logging.error(e)
        return bool(value)

    def getTitle(context, title):
        WebDriverWait(context.driver, 10).until(ExpectedConditions.title_is(title))
        logging.info("Page Title is :" + context.driver.title)
        return context.driver.title

    def clickElementOnList(context, by_locator, vehicle_group):
        try:
            time.sleep(5)
            locator = (By.XPATH, by_locator)
            WebDriverWait(context.driver, 10).until(ExpectedConditions.presence_of_all_elements_located(locator))
            vehicle_groups = context.driver.find_elements_by_xpath(by_locator)
            time.sleep(1)
            for group in vehicle_groups:
                logging.info("Groups are : " + group.text)
                if group.text == vehicle_group:
                    logging.info("Clicked On : " + group.text)
                    context.clickOnElementUsingActionChain(group)
                    return
                else:
                    context.scrollToElement(group)
        except StaleElementReferenceException as e:
            logging.error(e)

    def getElementTextInList(context, by_locator, text):
        try:
            locator = (By.XPATH, by_locator)
            WebDriverWait(context.driver, 10).until(ExpectedConditions.presence_of_all_elements_located(locator))
            vehicle_groups = context.driver.find_elements_by_xpath(by_locator)
            time.sleep(5)
            for group in vehicle_groups:
                logging.info("Groups are Read: " + group.text)
                if group.text == text:
                    logging.info("Clicked On : " + group.text)
                    return
                else:
                    context.scrollToElement(group)
        except StaleElementReferenceException as e:
            logging.error(e)

    def scrollToElement(context, by_locator):
        context.driver.execute_script("arguments[0].scrollIntoView(true);", by_locator)

    def clickOnElementByJS(context, element):
        context.driver.execute_script("arguments[0].click();", element)
        context.driver.implicitly_wait(10)

    def clickOnElementUsingActionChain(context, by_locator):
        logging.info(by_locator)
        action = ActionChains(context.driver)
        action.move_to_element(by_locator)
        action.click()
        action.perform()

    def clickOnElementUsingAction(context, element):
        logging.info(element)
        action = ActionChains(context.driver)
        action.move_to_element(element)
        action.click()
        action.perform()
        time.sleep(5)

    def element_creator(context, by_locator, locator_type):
        if locator_type.lower() == 'id':
            element = context.driver.find_element_by_id(by_locator)
            return element
        elif locator_type.lower() == 'xpath':
            element = context.driver.find_elements_by_xpath(by_locator)
            return element
        elif locator_type.lower() == 'name':
            element = context.driver.find_elements_by_name(by_locator)
            return element
        else:
            raise LocatorTypeUnidentifiedException

    def switch_to_window(context):
        time.sleep(5)
        current_window = context.driver.current_window_handle
        logging.info("Found Current Window : " + current_window)
        new_window_count = 0
        for window in context.driver.window_handles:
            logging.info("Found Windows : " + window)
            new_window_count = new_window_count + 1
        logging.info(new_window_count)
        new_window = context.driver.window_handles[new_window_count - 1]
        context.driver.switch_to_window(new_window)
        logging.info(new_window_count)

    def click_element_in_element(context, root_elm, sub_elm_one, text, sub_elm_two):
        time.sleep(2)
        list_data = context.driver.find_elements_by_xpath(root_elm + sub_elm_one)
        logging.info("************************* Came Here ****************")
        time.sleep(1)
        index = 0
        for group in list_data:
            index = index + 1
            logging.info("************ Stage Two **************************")
            if text == group.text:
                logging.info("**********************Came Inside the Loop ***************************")
                logging.info(index)
                element = context.driver.find_element_by_xpath(root_elm + "[" + str(index) + "]" + sub_elm_two)
                time.sleep(2)
                logging.info(element.is_selected())
                if not element.is_selected():
                    context.clickOnElementUsingAction(element)
                logging.info("Found Element : ")
                logging.info("Clicked on element input :" + group.text)
                break

    def get_text_on_element_in_element(context, root_elm, sub_elm_one, text, sub_elm_two):
        try:
            time.sleep(2)
            list_data = context.driver.find_elements_by_xpath(root_elm+sub_elm_one)

            time.sleep(8)
            index = 0
            for group in list_data:
                index = index + 1
                if text == group.text:
                    logging.info("**********************Came Inside the Loop ***************************")
                    logging.info(index)
                    element = context.driver.find_element_by_xpath(root_elm + "[" + str(index) + "]" + sub_elm_two)
                    time.sleep(2)
                    return str(element.text).strip().lower()
                    logging.info("Found Element : ")
                    logging.info("Returing element text :" + element.text)
                    break

        except Exception as e:
            logging.error(e)

    def click_all_elements_on_list(context, locator):
        time.sleep(3)
        list_data = context.driver.find_elements_by_xpath(locator)
        for element in list_data:
            logging.info("Navigated to element :" + element.text)
            context.clickOnElementUsingAction(element)

    def verifyTextOnElement(context, by_locator, text):
        logging.info("Waiting until the element to be visible to get text")
        try:
            value = WebDriverWait(context.driver, 10).until(
                ExpectedConditions.visibility_of_element_located(by_locator)).text
            if value.lower().strip() == text:
                logging.info("Text on the element matched")
                return
            else:
                logging.info("Text on the element did not matched")
                screenCapture.capture_screenshot(context=context, name="Get Text")
        except Exception as e:
            screenCapture.capture_screenshot(context=context, name="Get Text")
            logging.error(e)

    def verifyVisibilityFalse(context, text, by_locator):
        logging.info("Verifying the Text Visibility")
        try:
            locator = (By.XPATH, by_locator)
            WebDriverWait(context.driver, 10).until(ExpectedConditions.presence_of_all_elements_located(locator))
            vehicle_groups = context.driver.find_elements_by_xpath(by_locator)
            time.sleep(5)
            for group in vehicle_groups:
                logging.info("Groups are Read: " + group.text)
                if group.text == text:
                    logging.info("Clicked On : " + group.text)
                    return
                else:
                    context.scrollToElement(group)
        except Exception as e:
            logging.error(e)

    def xpath_by_text(context, text):
        xpath = "//*[contains(text(),'" + text + "')]"
        return By.XPATH, xpath
