import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


class WorkListPage(BasePage):
    vehicle_list_root_element = "//*[@id='vehicleTree_grid'] //tr"
    vehicle_list_name_element = "//span[@class='leaf-primary-name']"
    vehicle_list_radio_button = " //td //input"
    work_list_date = "//*[@id='detailsGridPVDetailsGrid'] //td[6]"

    work_data_date = (By.XPATH, "//*[@id='WorkOrderDetails'] //tr[3]/td[2]")
    work_data_vehicle = (By.XPATH, "//*[@id='WorkOrderDetails'] //tr[5]/td[2]")
    button_add_new_work = (By.ID, "btnNew")
    alert_placeholder = (By.ID, "alertPlaceholder")
    start_date = (By.ID, "Start")


    # checklist menu
    works_checklist_menu = (By.ID, "btnChecklistMenu")
    history_record_date = (By.XPATH, "//*[@id='rhsInfo'] //tr[2]/td[1]")
    history_record_status = (By.XPATH, "//*[@id='rhsInfo'] //tr[2]/td[2]/a")

    # new works menu
    new_works_menu = (By.ID, "btnNewWork")
    new_works_summary = (By.ID, "Summary")
    new_work_date = (By.ID, "SendNowButton")
    save_button = (By.ID, "saveButton")
    search_location = (By.ID, "locationSearchSpan")
    location_wizard_search = (By.ID, "search_txtSearch")
    search_button = (By.ID, "search_btnSearch")
    works_list_driver = "//*[@id='allocatedWorkContent'] //td[8]"
    confirm_button = (By.ID, "ok_confirm0")
    work_vehicle_list_root = "//*[@id='tree_grid'] //tr"

    work_button_menu = (By.ID, "btnWork")

    # dispatch menu
    dispatch_menu = (By.ID, "btnDispatch")
    allocated_list_root = "//*[@id='detailsGridPVAllocated'] //tr"
    allocated_list_summary = "//td[8]"
    allocated_list_checkbox = "//td[1]/input"

    unallocated_list_root = "//*[@id='detailsGridPVUnallocated'] //tr"
    deallocate_button = (By.ID, "btndeallocate")
    more_option_button = "//td[3]/i"
    delete_option = (By.XPATH, "(//span/span[contains(text(), 'Delete')])[2]")
    ok_confirm1 = (By.ID, "ok_confirm1")
    button_allocate = (By.ID, "btncommit")

    sequance_selector_table = (By.XPATH, "//*[@id='allocationResequencegrid'] //div[2] //tr[1]")
    insert_before_button = (By.ID, "btnInsertBefore")
    commit_button = (By.ID, "btnCommit")

    # message elements
    message_list_menu = (By.ID, "btnMessagesMenu")
    message_new_menu = (By.ID, "btnNewMessage")
    message_summary_list = "//*[@id='detailsGridPVDetailsGrid'] //tr //td[7]"


    def select_vehicle_from_list(context, vehicle):
        time.sleep(3)
        context.click_element_in_element(context.vehicle_list_root_element, context.vehicle_list_name_element, vehicle,
                                         context.vehicle_list_radio_button)

    def verify_work_list_visibility(context, date):
        time.sleep(3)
        context.getElementTextInList(context.work_list_date, date)

    def click_on_work_list_record(context, date):
        time.sleep(3)
        context.clickElementOnList(context.work_list_date, date)

    def validate_work_list_entry(context, date, vehicle):
        time.sleep(3)
        work_date = context.getTextOfElement(context.work_data_date)
        work_vehicle = context.getTextOfElement(context.work_data_vehicle)
        if work_vehicle.strip() == vehicle and work_date.strip() == date:
            logging.info("Found the Vehicle in Works List and data matched ")
            return
        else:
            logging.error("Vehicle and Works data didn't matched in the system")

    def navigate_to_checklist(context):
        time.sleep(3)
        context.clickOnElement(context.works_checklist_menu)
        context.switch_to_window()

    def validate_the_checklist_entry(context, date, status):
        time.sleep(3)
        checklist_date = context.getTextOfElement(context.history_record_date)
        checklist_status = context.getTextOfElement(context.history_record_status)
        if checklist_date.strip() == date and checklist_status.strip() == status:
            logging.info("Checklist Data has been matched")
            return
        else:
            logging.error("History Data did not matched")

    def navigate_back_to_work_list(context):
        time.sleep(1)
        context.clickOnElement(context.work_button_menu)

    def add_new_work(context, summary):
        time.sleep(2)
        context.sendKeysToElement(context.new_works_summary, summary)
        time.sleep(1)
        context.clickOnElement(context.new_work_date)
        time.sleep(1)
        context.clickOnElement(context.save_button)
        time.sleep(1)
        context.clickOnElement(context.confirm_button)
        time.sleep(1)

    def navigate_to_new_works(context):
        time.sleep(3)
        context.clickOnElement(context.new_works_menu)

    def verify_added_work(context, summary):
        time.sleep(10)
        context.getElementTextInList(context.works_list_driver, summary)

    def select_vehicle_from_work_list(context, vehicle):
        time.sleep(3)
        context.click_element_in_element(context.work_vehicle_list_root, context.vehicle_list_name_element, vehicle,
                                         context.vehicle_list_radio_button)

    def navigate_to_dispatch(context):
        time.sleep(3)
        context.clickOnElement(context.dispatch_menu)
        context.switch_to_window()

    def deallocate_work_from_vehicle(context, summary):
        time.sleep(3)
        context.click_element_in_element(context.allocated_list_root, context.allocated_list_summary, summary,
                                         context.allocated_list_checkbox)
        time.sleep(2)
        context.clickOnElement(context.deallocate_button)

    def verify_deallocated_work_in_available(context, summary):
        time.sleep(8)
        context.getElementTextInList(context.unallocated_list_root + context.allocated_list_summary, summary)

    def delete_work(context, summary):
        time.sleep(3)
        context.click_element_in_element(context.allocated_list_root, context.allocated_list_summary, summary,
                                         context.allocated_list_checkbox)
        time.sleep(2)
        context.click_element_in_element(context.allocated_list_root, context.allocated_list_summary, summary,
                                         context.more_option_button)
        time.sleep(2)
        context.clickOnElement(context.delete_option)
        time.sleep(1)
        context.clickOnElement(context.confirm_button)

    def allocate_work_to_vehicle(context, summary):
        time.sleep(3)
        context.click_element_in_element(context.unallocated_list_root, context.allocated_list_summary, summary,
                                         context.allocated_list_checkbox)
        time.sleep(2)
        context.clickOnElement(context.button_allocate)
        time.sleep(2)
        context.clickOnElement(context.sequance_selector_table)
        time.sleep(1)
        context.clickOnElement(context.insert_before_button)
        time.sleep(1)
        context.clickOnElement(context.commit_button)

    def verify_allocated_work_in_available(context, summary):
        time.sleep(5)
        context.getElementTextInList(context.allocated_list_root + context.allocated_list_summary, summary)

    def click_on_add_work_button(context):
        time.sleep(3)
        context.clickOnElement(context.button_add_new_work)

    def create_new_work_through_popup(context, summary, date):
        time.sleep(3)
        context.sendKeysToElement(context.new_works_summary, summary)
        if date.lower() == 'now':
            context.clickOnElement(context.new_work_date)
        else:
            context.sendKeysToElement(context.start_date, date)
        context.clickOnElement(context.save_button)
        context.clickOnElement(context.confirm_button)

    def verify_adding_work (context, message):
        message_txt = context.getTextOfElement(context.alert_placeholder)
        if message_txt.lower().strip() == message:
            logging.log("Work Record has been found")
            return
        else:
            logging.error("Unable to find the works")

    def navigate_to_messages_menu(context):
        time.sleep(3)
        context.clickOnElement(context.message_list_menu)
        context.switch_to_window()

    def navigate_to_new_message(context):
        time.sleep(3)
        context.clickOnElement(context.message_new_menu)

    def verify_whether_added_message_summary(context, summary):
        time.sleep(8)
        context.getElementTextInList(context.message_summary_list, summary)



