import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class LocationManagementPage(BasePage):
    # location types elements
    location_type_list = "//*[@id='locationTypesGrid'] //td"
    name_field = (By.ID, "Name")
    save_button = (By.ID, "updateButton")
    delete_button = (By.ID, "deleteButton")
    delete_confirm_button = (By.ID, "ok_confirm0")
    assign_button = (By.ID, "buttonAssign")
    edit_button = (By.ID, "editButton")

    # location group
    groups_assigned_list = "//*[@id='locationsingroupParViewMainDiv']//td"
    groups_available_list = "//*[@id='availableLocationsTree_grid'] //*[@class='leaf-primary-name']"
    group_unassign_button = (By.XPATH, "(//button[@type='button'])[6]")
    group_list = "//*[@id='locationGroupTree_grid'] //*[@class='leaf-primary-name']"
    group_save_button = (By.ID, "updateGroupButton")

    def add_new_location_group(context, name):
        time.sleep(3)
        context.sendKeysToElement(context.name_field, text=name)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_added_location_type(context, name):
        time.sleep(3)
        context.getElementTextInList(by_locator=context.location_type_list, text=name)

    def click_on_the_location_type_in_list(context, name):
        time.sleep(3)
        context.clickElementOnList(context.location_type_list, name)

    def delete_location(context):
        time.sleep(3)
        context.clickOnElement(context.delete_button)
        time.sleep(1)
        context.clickOnElement(context.delete_confirm_button)

    def verify_new_location_group_in_list(context, name):
        time.sleep(3)
        context.getElementTextInList(by_locator=context.group_list, text=name)

    def click_on_location_group(context, group):
        time.sleep(3)
        context.clickElementOnList(context.group_list, group)

    def assign_location_from_available(context, group):
        time.sleep(3)
        context.clickElementOnList(context.groups_available_list, group)
        time.sleep(2)
        context.clickOnElement(context.assign_button)

    def verify_location_assign_to_group(context, group):
        time.sleep(3)
        context.getElementTextInList(context.groups_assigned_list, group)
        time.sleep(2)

    def unassign_location_from_group(context, group):
        time.sleep(3)
        context.clickElementOnList(context.groups_assigned_list, group)
        time.sleep(2)
        context.clickOnElement(context.group_unassign_button)

    def verify_un_assigned_location_from_group(context, group):
        time.sleep(3)
        context.getElementTextInList(context.groups_available_list, group)
        time.sleep(2)

    def i_click_on_edit_button(context):
        time.sleep(2)
        context.clickOnElement(context.edit_button)

    def verify_assignment_remove(context, location):
        time.sleep(3)
        context.verifyVisibilityFalse(text=location, by_locator=context.group_list)

