import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


class UserManagePage(BasePage):
    # role management
    roles_add_role_button = (By.ID, "newButton")
    roles_edit_role_button = (By.ID, "editButton")
    roles_delete_roles_button = (By.ID, "deleteButton")
    roles_list_role_names = "//tr //td"
    roles_list_root_elm = "//*[@id='rightsTreeview_tv_active']"
    roles_list_labels = " //span[3]"
    roles_list_roles_inputs = "//input"

    # add new role
    add_role_name = (By.ID, "RoleName")
    add_role_user_see_all_driver_checkbox = "userCanSeeAllDriversCheckBox"
    add_role_user_see_all_vehicles_checkbox = "userCanSeeAllVehiclesCheckBox"
    add_role_save_button = (By.XPATH, "//button[contains(text(), 'Save')]")

    # user management
    user_username = (By.ID, "UserName")
    user_first_name = (By.ID, "FirstName")
    user_last_name = (By.ID, "LastName")
    user_password = (By.ID, "Password")
    user_conf_password = (By.ID, "passwordConfirm")
    user_phone = (By.ID, "Phone1")
    user_email_id = (By.ID, "Email1")

    def click_on_add_role_button(context):
        time.sleep(3)
        context.clickOnElement(context.roles_add_role_button)
        time.sleep(2)

    def add_user_role(context, role_name, all_vehicles, all_drivers):
        context.sendKeysToElement(context.add_role_name, role_name)
        if all_vehicles.lower() == 'yes':
            context.clickOnElementUsingAction(context.element_creator(
                context.add_role_user_see_all_vehicles_checkbox, "id"))
        if all_drivers.lower() == 'yes':
            context.clickOnElementUsingAction(
                context.element_creator(context.add_role_user_see_all_driver_checkbox, "id"))
        context.clickOnElementUsingAction(context.element_creator(context.add_role_save_button, "id"))
        time.sleep(3)

    def i_can_see_role_in_list(context, role_name):
        context.getElementTextInList(context.roles_list_role_names, role_name)

    def i_click_on_the_role_from_list(context, role_name):
        context.clickElementOnList(context.roles_list_role_names, role_name)

    def i_click_on_edit_button(context):
        time.sleep(3)
        context.clickOnElement(context.roles_edit_role_button)
        time.sleep(2)

    def i_change_the_permission_to_role(context, role):
        time.sleep(3)
        context.click_element_in_element(context.roles_list_root_elm,
                                         context.roles_list_role_names,
                                         role,
                                         context.roles_list_roles_inputs)

    def i_click_on_save_changes(context):
        time.sleep(3)
        context.clickOnElementUsingAction(context.element_creator(context.add_role_save_button, "id"))

    def i_click_on_add_user_button(context):
        time.sleep(3)
        context.clickOnElement(context.roles_add_role_button)

    def i_add_user_with_details(context, username, fname, lname, password, phone, email):
        time.sleep(3)
        context.sendKeysToElement(context.user_username, username)
        context.sendKeysToElement(context.user_first_name, fname)
        context.sendKeysToElement(context.user_last_name, lname)
        context.sendKeysToElement(context.user_password, password)
        context.sendKeysToElement(context.user_conf_password, password)
        context.sendKeysToElement(context.user_phone, phone)
        context.sendKeysToElement(context.user_email_id, email)
        time.sleep(2)
        context.clickOnElement(context.add_role_save_button)

    def i_see_username_in_the_list(context, username):
        time.sleep(4)
        context.getElementTextInList(context.roles_list_role_names, username)

    def click_on_user_in_list(context, username):
        time.sleep(1)
        context.clickElementOnList(context.roles_list_role_names, username)
