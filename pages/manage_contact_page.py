import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)

class ContactManagementPage(BasePage):
    # contact details eements
    contact_group_list = "//*[@id='contactGroupGrid'] //td"
    add_button = (By.ID, "addButton")
    group_name = (By.NAME, "Name")
    update_button = (By.ID, "updateButton")
    contact_name = (By.XPATH, "//*[@id='contactsDataGrid'] //div[2] //td[1]")
    phone = (By.NAME, "Phone")
    email = (By.NAME, "Email")
    contact_mobile = (By.XPATH, "//*[@id='contactsDataGrid'] //div[2] //td[3]")
    contact_email = (By.XPATH, "//*[@id='contactsDataGrid'] //div[2] //td[5]")
    contact_save_button = (By.ID, "updateContactGridButton")
    contact_delete_button = (By.XPATH, "//*[@id='contactsDataGrid'] //div[2] //button")
    contact_grid = "//*[@id='contactsDataGrid'] //div[2] //td"
    success_placeholder = (By.ID, "alertPlaceholder")

    def verify_visibility_in_list(context, name):
        time.sleep(2)
        context.getElementTextInList(by_locator=context.contact_group_list, text=name)

    def click_on_add_button_in_grid(context):
        time.sleep(2)
        context.clickOnElement(context.add_button)

    def add_contact_to_group(context, name, mobile, email):
        time.sleep(2)
        context.clickOnElement(context.contact_name)
        time.sleep(0.5)
        context.sendKeysToElement(context.group_name, name)

        context.clickOnElement(context.contact_mobile)
        time.sleep(0.5)
        context.sendKeysToElement(context.phone, mobile)

        context.clickOnElement(context.contact_email)
        time.sleep(0.5)
        context.sendKeysToElement(context.email, email)

        time.sleep(1)
        context.clickOnElement(context.contact_save_button)

    def verify_visibility_of_contact(context):
        time.sleep(2)
        value = context.getTextOfElement(context.success_placeholder)
        if value == 'Success':
            logging.info("Status Matched Success!!")
        else:
            logging.info("Status Mismatched Failed!!")

    def delete_contact_from_group(context):
        time.sleep(2)
        context.clickOnElement(context.contact_delete_button)
        time.sleep(1)
        context.clickOnElement(context.contact_save_button)

    def click_on_contact_in_group(context, name):
        time.sleep(2)
        context.clickElementOnList(by_locator=context.contact_group_list, vehicle_group=name)

    def add_contact_group(context, name):
        time.sleep(1)
        context.sendKeysToElement(context.group_name, name)
        time.sleep(1)
        context.clickOnElement(context.update_button)
