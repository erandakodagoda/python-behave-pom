import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class VehicleManagePage(BasePage):
    # common elements
    add_new_button = (By.ID, "newButton")
    save_button = (By.ID, "updateButton")
    update_save_button = (By.ID, "updateTemplateValuesButton")
    edit_button = (By.ID, "editButton")

    # diagnostic template elements
    diagnostic_temp_name = (By.ID, "Name")
    diagnostic_temp_list = "//span[@class='leaf-primary-name']"
    diagnostic_add_button = (By.ID, "addButton")
    diagnostic_grid = (By.XPATH, "//*[@class='gridControl'][1]")
    diagnostic_type_value = (By.XPATH, "//*[containts(text(), 'Abs. Engine Load %')]")
    diagnostic_type = (By.XPATH, "//*[@class='k-input']")

    diagnostic_operator = (By.XPATH, "//*[contains(text(), 'Equal To')]")

    # class elements
    class_list = "//tr/td"

    # operating hours elements
    operating_hours_temp_name = "//*[@id='operatingHoursDataGrid'] //tr/td[2]"

    # vehicle permissions elements
    permission_driver_list = "//*[@id='usersTree_grid'] //*[@class='leaf-primary-name']"
    permission_assign_button = (By.ID, "buttonAssign")
    permission_un_assign_button = (By.XPATH, "(//button[@type='button'])[3]")
    permission_assigned_group_list = "//*[@id='usersGroupTree_grid'] //*[@class='leaf-primary-name']"
    permission_available_group_list = "//*[@id='availableGroupsTree_grid'] //*[@class='leaf-primary-name']"

    # vehicle groups elements
    groups_save_button = (By.ID, "updateGroupButton")
    groups_group_list = "//*[@id='vehiclGroupTree_grid'] //*[@class='leaf-primary-name']"
    group_available_vehicles = "//*[@id='availableVehiclesTree_grid'] //*[@class='leaf-primary-name']"
    group_assigned_vehicles = "//*[@id='vigParViewMainDiv'] //td"
    groups_un_assign_button = (By.XPATH, "(//button[@type='button'])[8]")

    # vehicle details elements
    details_vehicle_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-primary-name']"
    detail_vehicle_name = (By.ID, "VehicleName")
    details_vehicle_id_field = (By.ID, "VehicleId")

    def click_on_add_new_template(context):
        time.sleep(3)
        context.clickOnElement(context.add_new_button)

    def add_new_template(context, template_name):
        time.sleep(3)
        context.sendKeysToElement(context.diagnostic_temp_name, template_name)
        context.clickOnElement(context.save_button)

    def verify_the_template_in_the_list(context, template_name):
        time.sleep(3)
        context.getElementTextInList(context.diagnostic_temp_list, template_name)

    def click_on_template_from_list(context, template_name):
        time.sleep(3)
        context.clickElementOnList(context.diagnostic_temp_list, template_name)

    def add_new_alert_for_template(context):
        time.sleep(3)
        context.clickOnElement(context.diagnostic_add_button)
        time.sleep(2)
        context.clickOnElement(context.diagnostic_grid)
        context.clickOnElement(context.diagnostic_type)
        context.clickOnElement(context.diagnostic_type_value)

        context.clickOnElement(context.diagnostic_grid)
        context.clickOnElement(context.diagnostic_type)
        context.clickOnElement(context.diagnostic_operator)

        context.clickOnElement(context.diagnostic_grid)
        context.clickOnElement(context.diagnostic_type)
        context.sendKeysToElement(context.diagnostic_type, "10")

        time.sleep(2)
        context.clickOnElement(context.update_save_button)

    def add_vehicle_class(context, class_name):
        time.sleep(3)
        context.sendKeysToElement(context.diagnostic_temp_name, class_name)
        context.clickOnElement(context.save_button)
        time.sleep(2)

    def verify_class_in_list(context, class_name):
        time.sleep(3)
        context.getElementTextInList(context.class_list, class_name)

    def add_new_operating_hours(context, template_name):
        time.sleep(3)
        context.sendKeysToElement(context.diagnostic_temp_name, template_name)
        context.clickOnElement(context.save_button)
        time.sleep(3)

    def verify_the_record_visibility(context, name):
        time.sleep(3)
        context.getElementTextInList(context.operating_hours_temp_name, name)

    def click_on_operating_record(context, name):
        time.sleep(3)
        context.clickElementOnList(context.operating_hours_temp_name, name)

    def click_on_the_driver_from_list(context, driver_name):
        time.sleep(3)
        context.clickElementOnList(context.permission_driver_list, driver_name)

    def add_group_to_driver(context, group_name):
        time.sleep(3)
        context.clickElementOnList(context.permission_available_group_list, group_name)
        time.sleep(2)
        context.clickOnElement(context.permission_assign_button)

    def verify_group_assign_to_driver(context, group_name):
        time.sleep(3)
        context.getElementTextInList(context.permission_assigned_group_list, group_name)
        time.sleep(2)

    def unassign_group_from_drivers(context, group_name):
        time.sleep(3)
        context.clickElementOnList(context.permission_assigned_group_list, group_name)
        time.sleep(2)
        context.clickOnElement(context.permission_un_assign_button)

    def verify_un_assigned_group_to_driver(context, group_name):
        time.sleep(3)
        context.clickElementOnList(context.permission_available_group_list, group_name)
        time.sleep(2)

    def click_on_add_group_button(context):
        time.sleep(3)
        context.clickOnElement(context.add_new_button)

    def add_new_group(context, group_name):
        time.sleep(3)
        context.sendKeysToElement(context.diagnostic_temp_name, group_name)
        time.sleep(1)
        context.clickOnElement(context.groups_save_button)

    def verify_added_group_in_the_list(context, group_name):
        time.sleep(3)
        context.getElementTextInList(context.groups_group_list, group_name)

    def add_vehicles_to_group(context, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.group_available_vehicles, vehicle)
        time.sleep(2)
        context.clickOnElement(context.permission_assign_button)

    def verify_vehicle_assign_to_group(context, vehicle):
        time.sleep(3)
        context.getElementTextInList(context.group_assigned_vehicles, vehicle)
        time.sleep(2)

    def unassign_vehicle_from_group(context, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.group_assigned_vehicles, vehicle)
        time.sleep(2)
        context.clickOnElement(context.groups_un_assign_button)

    def verify_un_assigned_vehicle_from_group(context, vehicle):
        time.sleep(3)
        context.getElementTextInList(context.group_available_vehicles, vehicle)
        time.sleep(2)

    def click_on_the_group_from_list(context, group_name):
        time.sleep(3)
        context.clickElementOnList(context.groups_group_list, group_name)

    def click_on_vehicle_from_vehicle_list(context, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.details_vehicle_list, vehicle)

    def verify_name_in_the_text_field(context, vehicle):
        time.sleep(2)
        context.verifyTextOnElement(context.detail_vehicle_name, vehicle)

    def add_vehicle_id_to_the_text_field(context, vehicle_id):
        time.sleep(2)
        context.sendKeysToElement(context.details_vehicle_id_field, vehicle_id)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_name_and_id(context, vehicle_id):
        time.sleep(3)
        context.verifyTextOnElement(context.details_vehicle_id_field, vehicle_id)
        time.sleep(2)

    def click_on_vehicle_class_from_the_list(context, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.class_list, vehicle)
        time.sleep(2)

    def click_on_edit_button(context):
        time.sleep(2)
        context.clickOnElement(context.edit_button)








