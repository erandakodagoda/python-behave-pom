import logging
import time
from pages.base_page import BasePage
from selenium.webdriver.common.by import By

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


class DashboardPage(BasePage):
    # dashboard common locators
    dashboard_alerts = (By.ID, "btnHideAlarmPopup")
    dashboard_user_profile_name = "//*[@id='mainToolbar'']/div"
    dashboard_logout = "//span[contains(text(),'Log out')]"
    dashboard_reports_menu = (By.ID, "btnReportsMenu")
    dashboard_manage_menu = (By.ID, "btnManageMenu")
    dashboard_status_menu = (By.ID, "btnStatusMenu")

    # dashboard vehicle menu locators
    dashboard_vehicles_menu = (By.ID, "vehicleTabLabel")
    vehicles_menu_vehicle_groups = "//div[@id='vehicleTab']//tr[contains(@class, 'tree-row')]//div//span[@class='leaf-primary-name']"
    vehicle_vehicle_label = (By.ID, "vehicleName")
    vehicle_current_driver = (By.ID, "statusCurDriver")
    vehicle_assigned_driver = (By.ID, "statusAssignedDriver")
    vehicle_driver_status = (By.ID, "statusDriverStatus")
    vehicle_run = (By.ID, "statusRunAssigned")
    vehicle_ignition = (By.ID, "statusIgnition")
    vehicle_status = (By.ID, "statusVehicleStatus")
    vehicle_terminal_state = (By.ID, "statusTerminalState")
    vehicle_gps_position = (By.ID, "statusGpsStateText")

    # dashboard driver menu locators
    dashboard_driver_menu = "driverTabLabel"
    drivers_menu_driver_runs = "//div[@id='driverTab']//tr[contains(@class, 'tree-row')]//div//span[@class='leaf-primary-name']"

    # dashboard run menu locators
    dashboard_run_menu = "runsTabLabel"
    run_menu_runs = "//div[@id='runsTab']//tr[contains(@class, 'tree-row')]//div//span[@class='leaf-primary-name']"

    # dashboard location menu locators
    dashboard_location_menu = "locationsTabLabel"
    location_menu_locations = "//div[@id='locationsTab']//tr[contains(@class, 'tree-row')]//div//span[@class='leaf-primary-name']"
    location_name = (By.ID, "locName")
    location_group = (By.ID, "locGroup")
    location_department = (By.ID, "locDepartment")
    location_type = (By.ID, "locType")
    location_pending = (By.ID, "locPending")

    # dashboard alert menu locators
    dashboard_alert_menu = "alertsTabLabel"
    alert_menu_alerts = "//div[@id='alerts_grid']//td[contains(@class, 'alertItem')]//div//div[@class='col-xs-12']"
    alert_message = (By.ID, "alertMessage")
    alert_vehicle = (By.ID, "alertVehicle")
    alert_fleet_id = (By.ID, "alertFleetId")

    # dashboard alarms menu locators
    dashboard_alarm_menu = "alarmsTabLabel"
    alarm_menu_alarms = "//div[@id='alarms_grid']//td[contains(@class, 'alertItem')]//div//div[@class='col-xs-12']"

    # dashboard works menu
    works_menu = (By.ID, "btnWorksMenu")

    # main screen dashboard menu
    dashboard_menu = (By.ID, "btnDashboardMenu")

    def __init__(context, driver):
        super().__init__(driver)

    def i_validate_the_dashboard_username(context, username):
        try:
            if context.verifyElementVisible(context.dashboard_alerts):
                context.clickOnElement(context.dashboard_alerts)
                value = context.getTextOfElement(context.dashboard_user_profile_name)
                if value == username:
                    print("{} {} {}".format(value, "matched with expected result of", username))
                else:
                    print("{} {} {}".format(value, "doesn't matched with expected result of", username))
            else:
                value = context.getTextOfElement(context.dashboard_user_profile_name)
                if value == username:
                    print("{} {} {}".format(value, "matched with expected result of", username))
                else:
                    print("{} {} {}".format(value, "doesn't matched with expected result of", username))
        except Exception as e:
            print(e)

    def navigate_to_vehicles_tab(context):
        context.clickOnElement(context.dashboard_vehicles_menu)

    def navigate_to_driver_tab(context):
        context.clickOnElementUsingAction(context.element_creator(context.dashboard_driver_menu, "id"))

    def verify_vehicle_group(context, group_name):
        context.clickElementOnList(context.vehicles_menu_vehicle_groups, group_name)

    def verify_driver_run(context, run):
        context.clickElementOnList(context.drivers_menu_driver_runs, run)

    def verify_vehicle_info(context, vehicle_name, current_driver, assigned_driver):
        time.sleep(10)
        logging.info(context.getTextOfElement(context.vehicle_vehicle_label) + "\n" + context.getTextOfElement(
            context.vehicle_current_driver) + "\n" + context.getTextOfElement(
            context.vehicle_assigned_driver))
        if vehicle_name == context.getTextOfElement(context.vehicle_vehicle_label) \
                or current_driver == context.getTextOfElement(context.vehicle_current_driver):
            logging.info("Data Matched")
        else:
            logging.info("Data does not matched")
            raise Exception

    def logout_from_application(context):
        context.clickOnElementUsingAction(context.dashboard_user_profile_name)
        context.clickOnElementUsingAction(context.dashboard_logout)

    def navigate_to_run_tab(context):
        context.clickOnElementUsingAction(context.element_creator(context.dashboard_run_menu, "ID"))

    def verify_run_in_runs(context, run):
        context.clickElementOnList(context.run_menu_runs, run)

    def verify_run(context, run):
        context.getElementTextInList(context.run_menu_runs, run)

    def navigate_to_location_tab(context):
        context.clickOnElementUsingAction(context.element_creator(context.dashboard_location_menu, "id"))

    def verify_location_in_locations(context, location):
        context.getElementTextInList(context.location_menu_locations, location)

    def click_on_location_from_list(context, location):
        context.clickElementOnList(context.location_menu_locations, location)

    def navigate_to_alert_tab(context):
        context.clickOnElementUsingAction(context.element_creator(context.dashboard_alert_menu, "id"))

    def verify_alert_in_alerts(context, alert):
        context.clickElementOnList(context.alert_menu_alerts, alert)

    def navigate_to_alarm_tab(context):
        context.clickOnElementUsingAction(context.element_creator(context.dashboard_alarm_menu, "id"))

    def verify_alarm_in_alarms(context, alarm):
        context.clickElementOnList(context.alarm_menu_alarms, alarm)

    def verify_location_info(context, name, group, name_type, department, pending):
        time.sleep(10)
        logging.info(context.getTextOfElement(context.location_name) + "=" + name + "\n" +
                     context.getTextOfElement(context.location_group) + "=" + group + "\n" +
                     context.getTextOfElement(context.location_type) + "=" + name_type + "\n" +
                     context.getTextOfElement(context.location_department) + "=" + department + "\n" +
                     context.getTextOfElement(context.location_pending) + "=" + pending)
        if name == context.getTextOfElement(context.location_name).rstrip():
            logging.info("Location Data Matched")
            return
        else:
            logging.info("Location Data did Not Matched")

    def verify_alert_info(context, message, vehicle, fleet_id):
        time.sleep(10)
        logging.info(context.getTextOfElement(context.alert_message).rstrip() + "\n" +
                     context.getTextOfElement(context.alert_vehicle) + "\n" +
                     context.getTextOfElement(context.alert_fleet_id))
        if message == context.getTextOfElement(context.alert_message).rstrip() \
                and vehicle == context.getTextOfElement(context.alert_vehicle).rstrip() \
                and fleet_id == context.getTextOfElement(context.alert_fleet_id).rstrip():
            logging.info("Alert Data Matched")
            return
        else:
            logging.error("Alert Data Did not matched")

    def navigate_to_reports_menu(context):
        context.clickOnElement(context.dashboard_reports_menu)
        context.switch_to_window()

    def click_on_manage_menu(context):
        context.clickOnElement(context.dashboard_manage_menu)
        context.switch_to_window()

    def click_on_status_menu(context):
        context.i_close_the_alert_if_appeared()
        context.clickOnElement(context.dashboard_status_menu)
        context.switch_to_window()

    def i_close_the_alert_if_appeared(context):
        try:
            if context.verifyElementVisible(context.dashboard_alerts):
                context.clickOnElement(context.dashboard_alerts)
                return
            else:
                print("No Alert Found")
                return
        except Exception as e:
            print("Exception found")

    def navigate_to_works_menu(context):
        context.clickOnElement(context.works_menu)
        context.switch_to_window()

    def navigate_to_status_menu(context):
        context.clickOnElement(context.status_menu)
        context.switch_to_window()

    def navigate_to_dashboard_menu(context):
        context.clickOnElement(context.dashboard_menu)
        context.switch_to_window()


