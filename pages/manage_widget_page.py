import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class WidgetManagementPage(BasePage):
    widget_vehicle_list_root = "//*[@id='popUpTree_grid']"
    widget_sub_elm_one = "//*[@class='leaf-primary-name']"
    widget_sub_elm_two = "//input"
    update_button = (By.ID, "updateButton")
    name_field = (By.ID, "Name")
    header_field = (By.ID, "Header")
    widget_list = "//*[@id='widgetDataGrid'] //tr //td[2]"

    def add_widget(context, name, header, vehicle):
        time.sleep(3)
        context.click_element_in_element(context.widget_vehicle_list_root,
                                         context.widget_sub_elm_one,
                                         vehicle,
                                         context.widget_sub_elm_two)
        time.sleep(1)
        context.sendKeysToElement(context.name_field, name)
        context.sendKeysToElement(context.header_field, header)

        context.clickOnElement(context.update_button)
        time.sleep(3)

    def verify_widget_in_list(context, name):
        time.sleep(2)
        context.getElementTextInList(context.widget_list, name)

    def click_on_widget_from_list(context, name):
        time.sleep(3)
        context.clickElementOnList(context.widget_list, name)





