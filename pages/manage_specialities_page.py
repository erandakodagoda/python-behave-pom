import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class SpecialityManagementPage(BasePage):
    # speciality detail elements
    name_field = (By.ID, "Name")
    update_button = (By.ID, "updateButton")
    specialities_grid = "//*[@id='specialitiesDataGrid'] //td[3]"
    edit_button = (By.ID, "editButton")


    def add_specaility(context, name):
        time.sleep(3)
        context.sendKeysToElement(context.name_field, name)
        context.clickOnElement(context.update_button)

    def verify_speciality(context, name):
        time.sleep(3)
        context.getElementTextInList(context.specialities_grid, name)

    def click_on_speciality_from_grid(context, name):
        time.sleep(3)
        context.clickElementOnList(context.specialities_grid, name)

    def click_on_edit_button(context):
        time.sleep(2)
        context.clickOnElement(context.edit_button)

