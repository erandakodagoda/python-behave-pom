import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ReportsPage(BasePage):
    # reports common
    reports_vehicles_menu = (By.ID, "menuVehicles")
    reports_filters_category = "//div[contains(@class,'row report-selection')] //div[1] //span[@class='k-widget k-dropdown']"
    reports_filters_type = "//div[contains(@class,'row report-selection')] //div[2] //span[@class='k-widget k-dropdown']"
    reports_filters_report = "//div[contains(@class,'row report-selection')] //div[3] //span[@class='k-widget k-dropdown']"
    reports_vehicles = "//tr[@class='tree-row leaf-row']"
    reports_vehicles_name = "//span[@class='leaf-primary-name']"
    reports_vehicles_input = "//td[4]/input"
    filters_individual_filter = (By.XPATH, "//*[contains(text(), 'Individual')]")
    filters_detailed_filter = (By.XPATH, "//*[contains(text(), 'Detailed')]")
    filters_multi_filter = (By.XPATH, "//*[contains(text(), 'Multi')]")
    filters_all_activity = (By.XPATH, "//*[contains(text(), 'All Activity')]")
    filters_trip = (By.XPATH, "//*[contains(text(), 'Trip')]")

    reports_start_date = (By.ID, "startTime")
    reports_end_date = (By.ID, "stopTime")
    reports_generate_button = (By.ID, "btnGenerateReport")

    def navigate_to_vehicle_reports_menu(context):
        context.clickOnElement(context.reports_vehicles_menu)

    def apply_filters(context, filter_type, report_type, report_name):
        time.sleep(3)
        # select report category
        context.driver.find_element_by_xpath(context.reports_filters_category).click()
        time.sleep(1)
        if filter_type.lower() == 'individual':
            context.clickOnElement(context.filters_individual_filter)
        elif filter_type.lower() == 'multi':
            context.clickOnElement(context.filters_multi_filter)
        # select report group
        context.driver.find_element_by_xpath(context.reports_filters_type).click()
        time.sleep(1)
        if report_type.lower() == 'detailed':
            context.clickOnElement(context.filters_detailed_filter)
        # select report
        context.driver.find_element_by_xpath(context.reports_filters_report).click()
        time.sleep(1)
        if report_name.lower() == 'all activity':
            context.clickOnElement(context.filters_all_activity)
        elif report_name.lower() == 'trip':
            context.clickOnElement(context.filters_trip)

    def select_vehicles(context, vehicle_id):
        context.click_element_in_element(context.reports_vehicles, context.reports_vehicles_name, vehicle_id,
                                         context.reports_vehicles_input)

    def insert_start_date_and_end_date(context, start_date, end_date):
        context.sendKeysToElement(context.reports_start_date, start_date)
        context.sendKeysToElement(context.reports_end_date, end_date)

    def click_on_generate_button(context):
        context.clickOnElement(context.reports_generate_button)
        time.sleep(5)
        context.switch_to_window()

    def i_see_vehicle_in_the_report(context, text):
        element = (By.XPATH, "//*[contains(text(),'" + text + "')]")
        elm_text = context.getTextOfElement(element)
        if elm_text == text:
            print("{} and {} data matched :".format(text, elm_text))
        else:
            print("{} and {} data did not matched :".format(text, elm_text))
            raise Exception
