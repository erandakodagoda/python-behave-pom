import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class MyDashboardPage(BasePage):
    add_widget_button = (By.ID, "add-widgets")
    add_widget_list = "//*[@id='grdWidgets']"
    add_widget_list_name = "//td[2]"
    add_widget_list_input = "//input"
    save_button = (By.ID, "btnSave")
    widget_header = "//*[@id='dashboard'] //div/span[2]"

    delete_widget_button_list = "//*[@id='dashboard'] "
    widget_list_close = "//div/span[1]"
    widget_name_list = "//div/span[2]"
    confirm_button = (By.ID, "ok_confirm0")

    def add_widget(context, widget):
        time.sleep(5)
        context.clickOnElement(context.add_widget_button)
        time.sleep(1)
        context.click_element_in_element(context.add_widget_list, context.add_widget_list_name, widget,
                                         context.add_widget_list_input)
        time.sleep(3)
        context.clickOnElement(context.save_button)

    def verify_added_widget(context, widget):
        time.sleep(3)
        context.getElementTextInList(context.widget_header, widget)

    def delete_widget(context, widget):
        time.sleep(2)
        context.click_element_in_element(context.delete_widget_button_list, context.widget_name_list, widget,
                                         context.widget_list_close)
        time.sleep(1)
        context.clickOnElement(context.confirm_button)
        time.sleep(2)
