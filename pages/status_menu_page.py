import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


class StatusPage(BasePage):
    # status menus common
    status_menu_grouped = (By.ID, "menuGrouped")
    status_menu_ungrouped = (By.ID, "menuUngrouped")
    status_menu_services = (By.ID, "menuServices")

    # grouped menu
    grouped_title = (By.XPATH, "//div[contains(text(),'Grouped')]")
    grouped_group_names = "//span[@class='leaf-primary-name']"
    grouped_alert_names = "//td[@class='name']"

    # ungrouped menu
    ungrouped_title = (By.XPATH, "//div[contains(text(),'Ungrouped')]")

    # services menu
    services_title = (By.XPATH, "//div[contains(text(),'Services')]")
    services_vehicle_names = (By.XPATH, "//*[@data-field='vehicleName']")
    services_operational_status_btn = (By.ID, "btnSetOperational")
    services_table_row = "//tr[@role='row']"
    services_operational_status = "//td[3]"
    service_table_vehicle_name = "//td[1]"
    service_table_details = "//td[16]"

    def i_click_on_grouped_menu(context):
        context.clickOnElement(context.status_menu_grouped)
        if context.verifyElementVisible(context.grouped_title):
            print("Element Displayed")
            return
        else:
            print("Element is not visible")

    def i_see_group_name_in_the_data(context, group_name):
        context.getElementTextInList(context.grouped_group_names, group_name)

    def i_see_alerts_in_the_data(context, alert):
        context.getElementTextInList(context.grouped_alert_names, alert)

    def i_click_on_ungrouped(context):
        context.clickOnElement(context.status_menu_ungrouped)
        if context.verifyElementVisible(context.ungrouped_title):
            print("Element Displayed")
            return
        else:
            print("Element is not visible")

    def i_click_on_services_menu(context):
        context.clickOnElement(context.status_menu_services)
        if context.verifyElementVisible(context.services_title):
            print("Element Displayed")
            return
        else:
            print("Element is not visible")

    def i_click_on_vehicle_from_list(context, vehicle):
        context.clickElementOnList(context.service_table_vehicle_name, vehicle)

    def i_change_the_operational_status(context, status):
        value = context.get_text_on_element_in_element(context.services_table_row,
                                               context.service_table_vehicle_name,
                                               status,
                                               context.services_operational_status)
        if value == status.lower():
            logging.info("Operational Status and Actual Status Seems to be matched \n"
                         "Ignoring without clicking on the button")
        else:
            logging.info("Clicking on Operational Status Button")
            context.clickOnElement(context.services_operational_status_btn)

    def i_click_on_details_page(context, vehicle):
        context.scrollToElement(context.element_creator(context.service_table_details, "xpath"))
        context.click_element_in_element(context.services_table_row,
                                         context.service_table_vehicle_name,
                                         vehicle,
                                         context.service_table_details)
        context.switch_to_window()
        try:
            time.sleep(8)
        except Exception as e:
            logging.error(e)
