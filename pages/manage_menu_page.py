import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ManagePage(BasePage):
    manage_drivers_menu = (By.ID, "driverMenu")
    manage_vehicles_menu = (By.ID, "vehicleMenu")
    manage_menus_page_titles = (By.XPATH, "//*[@id='panelHeader']  //td[@class='panel-title']")
    manage_users = (By.ID, "userMenu")
    manage_runs = (By.ID, "runMenu")

    # manage page drivers elements
    drivers_detail_menu = (By.ID, "driverDetails")
    drivers_tags_menu = (By.ID, "driverManageRFTags")
    drivers_group_menu = (By.ID, "driverGroups")
    drivers_permission_menu = (By.ID, "driverPermissions")
    drivers_assignment_menu = (By.ID, "driverManageAssignments")

    # manage page vehicles elements
    vehicles_detail_menu = (By.ID, "vehicleDetails")
    vehicles_diagnostic_template = (By.ID, "VehicleDiagnostics")
    vehicles_classes = (By.ID, 'vehicleClasses')
    vehicle_operating_hours = (By.ID, "vehicleOperatingHours")
    vehicles_permission = (By.ID, "vehiclePermissions")
    vehicle_groups = (By.ID, "vehicleGroups")
    vehicle_details = (By.ID, "vehicleDetails")

    # user menus elements
    user_details_menu = (By.ID, "userDetails")
    user_roles_rights_menu = (By.ID, "userRolesAndRights")

    # manage run elements
    run_details_menu = (By.ID, "runDetails")
    run_groups_menu = (By.ID, "runGroups")
    run_permission_menu = (By.ID, "runPermissions")
    run_assignments_menu = (By.ID, "runManageAssignments")

    # location manage menus
    location_manage_menu = (By.ID, "locationMenu")
    location_type_menu = (By.ID, "locationTypes")
    location_group_menu = (By.ID, "locationGroups")

    # department manage menus
    department_manage_menu = (By.ID, "departmentMenu")
    department_details_menu = (By.ID, "departmentDetails")

    # contacts manage manus
    contacts_manage_menu = (By.ID, "contactMenu")
    contacts_details_menu = (By.ID, "contactDetails")

    # services manage menus
    service_menu = (By.ID, "servicesMenu")
    service_details_menu = (By.ID, "serviceDetails")
    service_schedule_menu = (By.ID, "serviceSchedules")

    # specialities manage menu
    speciality_menu = (By.ID, "specialityMenu")
    speciality_details_menu = (By.ID, "specialityDetails")

    # widget manage menu
    widget_menu = (By.ID, "widgetMenu")
    widget_creator_menu = (By.ID, "widgetDetails")

    # tools manage menu
    tools_menu = (By.ID, "toolsMenu")
    tools_consumption_menu = (By.ID, "fuelConsumption")
    tools_fuel_import_menu = (By.ID, "fuelImport")

    def navigate_to_driver_details(context):
        time.sleep(3)
        context.clickOnElement(context.manage_drivers_menu)
        time.sleep(1)
        context.clickOnElement(context.drivers_detail_menu)

    def navigate_to_vehicle_details(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicles_detail_menu)

    def verify_the_page_title(context, title):
        text = context.getTextOfElement(context.manage_menus_page_titles)
        if text.strip() == title:
            print("{} and {} data matched :".format(text, title))
        else:
            print("{} and {} data doesn't matched :".format(text, title))

    def click_on_user_menu_roles(context):
        time.sleep(3)
        context.clickOnElement(context.manage_users)
        time.sleep(1)
        context.clickOnElement(context.user_roles_rights_menu)
        time.sleep(3)

    def i_click_on_user_detail_menu(context):
        time.sleep(3)
        context.clickOnElement(context.manage_users)
        time.sleep(1)
        context.clickOnElement(context.user_details_menu)
        time.sleep(2)

    def navigate_to_vehicle_diagnostic_temp(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicles_diagnostic_template)
        time.sleep(2)

    def navigated_to_vehicle_classes(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicles_classes)
        time.sleep(2)

    def navigated_to_operating_hours(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicle_operating_hours)

    def navigate_to_vehicle_permission(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicles_permission)

    def navigate_to_vehicle_groups(context):
        time.sleep(3)
        context.clickOnElement(context.manage_vehicles_menu)
        time.sleep(1)
        context.clickOnElement(context.vehicle_groups)

    def navigate_to_drivers_tags(context):
        time.sleep(3)
        context.clickOnElement(context.manage_drivers_menu)
        time.sleep(1)
        context.clickOnElement(context.drivers_tags_menu)

    def navigated_to_drivers_group(context):
        time.sleep(3)
        context.clickOnElement(context.manage_drivers_menu)
        time.sleep(1)
        context.clickOnElement(context.drivers_group_menu)

    def navigated_to_drivers_permission(context):
        time.sleep(3)
        context.clickOnElement(context.manage_drivers_menu)
        time.sleep(1)
        context.clickOnElement(context.drivers_permission_menu)

    def navigated_to_drivers_assignments(context):
        time.sleep(3)
        context.clickOnElement(context.manage_drivers_menu)
        time.sleep(1)
        context.clickOnElement(context.drivers_assignment_menu)

    def navigated_ro_runs_details(context):
        time.sleep(3)
        context.clickOnElement(context.manage_runs)
        time.sleep(1)
        context.clickOnElement(context.run_details_menu)

    def navigated_to_runs_groups(context):
        time.sleep(3)
        context.clickOnElement(context.manage_runs)
        time.sleep(1)
        context.clickOnElement(context.run_groups_menu)

    def navigated_to_runs_permissions(context):
        time.sleep(3)
        context.clickOnElement(context.manage_runs)
        time.sleep(1)
        context.clickOnElement(context.run_permission_menu)

    def navigated_to_runs_assignments(context):
        time.sleep(3)
        context.clickOnElement(context.manage_runs)
        time.sleep(1)
        context.clickOnElement(context.run_assignments_menu)

    def navigate_to_location_types(context):
        time.sleep(3)
        context.clickOnElement(context.location_manage_menu)
        time.sleep(1)
        context.clickOnElement(context.location_type_menu)

    def navigate_to_location_group(context):
        time.sleep(3)
        context.clickOnElement(context.location_manage_menu)
        time.sleep(1)
        context.clickOnElement(context.location_group_menu)

    def navigate_to_department_details(context):
        time.sleep(3)
        context.clickOnElement(context.department_manage_menu)
        time.sleep(1)
        context.clickOnElement(context.department_details_menu)

    def navigate_to_contacts_details(context):
        time.sleep(3)
        context.clickOnElement(context.contacts_manage_menu)
        time.sleep(1)
        context.clickOnElement(context.contacts_details_menu)

    def navigate_to_service_details(context):
        time.sleep(3)
        context.clickOnElement(context.service_menu)
        time.sleep(1)
        context.clickOnElement(context.service_details_menu)

    def navigate_to_service_schedules(context):
        time.sleep(3)
        context.clickOnElement(context.service_menu)
        time.sleep(1)
        context.clickOnElement(context.service_schedule_menu)

    def navigate_to_speciality_detail(context):
        time.sleep(3)
        context.clickOnElement(context.speciality_menu)
        time.sleep(1)
        context.clickOnElement(context.speciality_details_menu)

    def navigate_to_widgets_creator(context):
        time.sleep(3)
        context.clickOnElement(context.widget_menu)
        time.sleep(1)
        context.clickOnElement(context.widget_creator_menu)

    def navigate_to_fuel_consumption(context):
        time.sleep(3)
        context.clickOnElement(context.tools_menu)
        time.sleep(1)
        context.clickOnElement(context.tools_consumption_menu)



