import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class DepartmentManagementPage(BasePage):
    send_email_alerts_option = (By.XPATH, "(//*[@id='vehicleAlertsPVEmailSettings'] //div/label)[3]")
    vehicle_alerts_to_group = (By.XPATH, "//*[@id='vehicleAlertsPVcontactsTable'] //span")
    name_field = (By.ID, "Name")
    save_button = (By.ID, "updateButton")
    department_list = "//*[@id='departmentGrid'] //td"
    update_alerts_button = (By.ID, "updateAlertButton")

    def create_department(context, name):
        time.sleep(1)
        context.sendKeysToElement(context.name_field, name)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_created_department(context, name):
        time.sleep(2)
        context.getElementTextInList(by_locator=context.department_list, text=name)

    def update_department_details(context, contact):
        time.sleep(2)
        context.clickOnElement(context.send_email_alerts_option)
        time.sleep(1)
        context.clickOnElement(context.vehicle_alerts_to_group)
        time.sleep(1)
        context.clickOnElement(context.xpath_by_text(contact))
        time.sleep(1)
        context.clickOnElement(context.update_alerts_button)

    def click_on_department_in_list(context, name):
        time.sleep(2)
        context.clickElementOnList(context.department_list, name)


