import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ManageDriverPage(BasePage):
    # common elements
    save_button = (By.ID, "updateButton")
    new_button = (By.ID, "newButton")
    add_new_tag_button = (By.ID, "newDriverIdTagButton")

    # tags elements
    tags_tag_id_field = (By.ID, "RfTagSerial")
    tags_list = "//*[@id='rfTagsTree_grid'] //*[@class='leaf-primary-name']"

    # details elements
    details_short_name = (By.ID, "ShortName")
    details_first_name = (By.ID, "FirstName")
    details_last_name = (By.ID, "LastName")
    driver_list = "//*[@id='driversTree'] //*[@class='leaf-primary-name']"
    details_phone_number = (By.ID, "Phone1")
    details_driver_tag_text = (By.ID, "driverIdTagTextBox")
    driver_tag_driver_select = (By.XPATH, "(//*[@id='driverIdTagPopUpWindow']  //*[@class='row'][4] //span)[1]")

    # driver group element
    driver_group_list = "//*[@id='driverGroupTree_grid'] //*[@class='leaf-primary-name']"
    group_available_list = "//*[@id='availableDriversTree_grid'] //*[@class='leaf-primary-name']"
    group_assigned_list = "//*[@id='driveringroupParViewMainDiv'] //td"
    assign_button = (By.ID, "buttonAssign")
    unassign_button =(By.XPATH, "(//button[@type='button'])[8]")

    # driver permission elements
    permission_users_list = "//*[@id='usersTree_grid'] //*[@class='leaf-primary-name']"
    permission_assigned_user_groups = "//*[@id='usersGroupTree_grid'] //*[@class='leaf-primary-name']"
    permission_available_user_groups = "//*[@id='availableGroupsTree_grid'] //*[@class='leaf-primary-name']"
    permission_un_assign_button = (By.XPATH, "(//button[@type='button'])[3]")

    # driver assignment elements
    assignments_vehicle_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-primary-name']"
    assignments_driver_list = "//*[@id='driversTree_grid'] //*[@class='leaf-primary-name']"
    assignment_add_button = (By.XPATH, "(//*[@id='mainContent'] //button)[2]")
    assignment_assigned_drivers_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-middle-name']"

    def add_new_driver_tag(context, tag):
        time.sleep(3)
        context.sendKeysToElement(context.tags_tag_id_field, tag)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_the_tag_in_the_list(context, tag):
        time.sleep(3)
        context.getElementTextInList(context.tags_list, tag)

    def click_on_add_button(context):
        time.sleep(3)
        context.clickOnElement(context.new_button)

    def add_new_driver(context, short_name, first_name, last_name):
        time.sleep(3)
        context.sendKeysToElement(context.details_short_name, short_name)
        context.sendKeysToElement(context.details_first_name, first_name)
        context.sendKeysToElement(context.details_last_name, last_name)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_added_driver_in_the_list(context, short_name):
        time.sleep(3)
        context.getElementTextInList(context.driver_list, short_name)

    def click_non_driver_from_list(context, short_name):
        time.sleep(3)
        context.clickElementOnList(context.driver_list, short_name)

    def update_phone_number_of_driver(context, phone):
        time.sleep(3)
        context.sendKeysToElement(context.details_phone_number, phone)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_driver_number(context, phone):
        time.sleep(3)
        context.verifyTextOnElement(context.details_phone_number, phone)

    def click_on_add_tag(context):
        time.sleep(3)
        context.clickOnElement(context.add_new_tag_button)

    def add_tag_to_driver(context, tag):
        time.sleep(3)
        context.sendKeysToElement(context.details_driver_tag_text, tag)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def verify_driver_group_in_the_list(context, group):
        time.sleep(3)
        context.getElementTextInList(context.driver_group_list, group)

    def click_on_driver_group(context, group):
        time.sleep(3)
        context.clickElementOnList(context.driver_group_list, group)

    def assign_driver_from_available(context, driver):
        time.sleep(3)
        context.clickElementOnList(context.group_available_list, driver)
        time.sleep(2)
        context.clickOnElement(context.assign_button)

    def verify_driver_assign_to_group(context, driver):
        time.sleep(3)
        context.getElementTextInList(context.group_assigned_list, driver)
        time.sleep(2)

    def unassign_driver_from_group(context, driver):
        time.sleep(3)
        context.clickElementOnList(context.group_assigned_list, driver)
        time.sleep(2)
        context.clickOnElement(context.unassign_button)

    def verify_un_assigned_driver_from_group(context, driver):
        time.sleep(3)
        context.getElementTextInList(context.group_available_list, driver)
        time.sleep(2)

    def click_on_users_in_list(context, user):
        time.sleep(3)
        context.clickElementOnList(context.permission_users_list, user)

    def assign_driver_group_from_available(context, group):
        time.sleep(3)
        context.clickElementOnList(context.permission_available_user_groups, group)
        time.sleep(2)
        context.clickOnElement(context.assign_button)

    def verify_driver_group_assign_to_group(context, group):
        time.sleep(3)
        context.getElementTextInList(context.permission_assigned_user_groups, group)
        time.sleep(2)

    def unassign_driver_group_from_group(context, group):
        time.sleep(3)
        context.clickElementOnList(context.permission_assigned_user_groups, group)
        time.sleep(2)
        context.clickOnElement(context.permission_un_assign_button)

    def verify_un_assigned_driver_group_from_group(context, group):
        time.sleep(3)
        context.getElementTextInList(context.permission_available_user_groups, group)
        time.sleep(2)

    def assigned_driver_to_vehicle(context, driver, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.assignments_driver_list, driver)
        time.sleep(1)
        context.clickElementOnList(context.assignments_vehicle_list, vehicle)
        time.sleep(1)

    def click_on_assign_driver_button(context):
        time.sleep(2)
        context.clickOnElement(context.assignment_add_button)

    def verify_driver_assignment(context, driver):
        time.sleep(3)
        context.getElementTextInList(context.assignment_assigned_drivers_list, driver)












