import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ToolsManagementPage(BasePage):
    vehicle_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-primary-name']"
    purchase_date = (By.ID, "purchaseDate")
    update_button = (By.ID, "updateButton")
    total_fuel_ltr = (By.XPATH, "//*[@id='fuelPurchasesDataGrid'] //tr //td[2]")
    fuel_date = (By.XPATH, "//*[@id='fuelPurchasesDataGrid'] //tr //td[1]")
    save_button = (By.ID, "updateContactGridButton")
    delete_record_button = (By.XPATH, "//*[@id='fuelPurchasesDataGrid'] //tr //td //button")

    def click_on_vehicle(context, vehicle):
        time.sleep(2)
        context.clickElementOnList(context.vehicle_list, vehicle)

    def save_fuel_consumption_record(context, on_date):
        time.sleep(2)
        context.sendKeysToElement(context.purchase_date, on_date)
        context.clickOnElement(context.update_button)

    def verify_added_consumption_record(context, on_date):
        time.sleep(2)
        context.getElementTextInList(context.fuel_date, on_date)

    def edit_consumption_record(context, on_date, new_date):
        time.sleep(2)
        context.clickElementOnList(context.fuel_date, on_date)
        time.sleep(1)
        context.sendKeysToElement(context.fuel_date, new_date)
        time.sleep(1)
        context.clickOnElement(context.save_button)

    def delete_consumption_record(context):
        time.sleep(3)
        context.clickOnElement(context.delete_record_button)
        time.sleep(1)
        context.clickOnElement(context.save_button)


