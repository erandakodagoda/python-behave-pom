import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class ManageRunPage(BasePage):
    # common elements
    run_list = "//*[@id='runTree_grid'] //*[@class='leaf-primary-name']"
    add_button = (By.ID, "newButton")
    name_field = (By.ID, "Name")
    update_button = (By.ID, "updateButton")
    assign_button = (By.ID, "buttonAssign")

    # run groups elements
    groups_assigned_list = "//*[@id='runingroupParViewMainDiv']//td"
    groups_available_list = "//*[@id='availableRunsTree'] //*[@class='leaf-primary-name']"
    group_unassign_button = (By.XPATH, "(//button[@type='button'])[8]")
    group_list = "//*[@id='runGroupTree_grid'] //*[@class='leaf-primary-name']"
    group_save_button = (By.ID, "updateGroupButton")

    # run permission elements
    permission_user_list = "//*[@id='usersTree_grid'] //*[@class='leaf-primary-name']"
    permission_assigned_list = "//*[@id='usersGroupTree_grid'] //*[@class='leaf-primary-name']"
    permission_available_list = "//*[@id='availableGroupsTree_grid'] //*[@class='leaf-primary-name']"
    permission_unassign_button = (By.XPATH, "(//button[@type='button'])[3]")

    # run assignment elements
    assignments_vehicle_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-primary-name']"
    assignments_run_list = "//*[@id='runsTree'] //*[@class='leaf-primary-name']"
    assignment_add_button = (By.XPATH, "(//*[@id='mainContent'] //button)[2]")
    assignment_assigned_runs_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-middle-name']"
    root_element = "//*[@id='vehiclesTree_grid']"
    element_one = "//*[@class='leaf-middle-name']"
    element_two = "//td[3]/span"


    def click_on_add_button(context):
        time.sleep(3)
        context.clickOnElement(context.add_button)

    def add_new_run(context, run):
        time.sleep(3)
        context.sendKeysToElement(context.name_field, run)
        time.sleep(2)
        context.clickOnElement(context.update_button)

    def verify_added_run_in_list(context, run):
        time.sleep(3)
        context.getElementTextInList(context.run_list, run)

    def click_on_run_from_list(context, run):
        time.sleep(3)
        context.clickElementOnList(context.run_list, run)

    def click_on_run_group(context, run):
        time.sleep(3)
        context.clickElementOnList(context.group_list, run)

    def assign_run_from_available(context, run):
        time.sleep(3)
        context.clickElementOnList(context.groups_available_list, run)
        time.sleep(2)
        context.clickOnElement(context.assign_button)

    def verify_run_assign_to_group(context, run):
        time.sleep(3)
        context.getElementTextInList(context.groups_assigned_list, run)
        time.sleep(2)

    def unassign_run_from_group(context, run):
        time.sleep(3)
        context.clickElementOnList(context.groups_assigned_list, run)
        time.sleep(2)
        context.clickOnElement(context.group_unassign_button)

    def verify_un_assigned_run_from_group(context, run):
        time.sleep(3)
        context.getElementTextInList(context.groups_available_list, run)
        time.sleep(2)

    def verify_added_run_group(context, group):
        time.sleep(3)
        context.getElementTextInList(context.group_list, group)

    def add_new_run_group(context, group):
        time.sleep(3)
        context.sendKeysToElement(context.name_field, group)
        time.sleep(1)
        context.clickOnElement(context.group_save_button)

    def click_on_driver_from_list(context, driver):
        time.sleep(3)
        context.clickElementOnList(by_locator=context.permission_user_list, vehicle_group=driver)
        time.sleep(1)

    def assign_run_permission_from_available(context, run):
        time.sleep(3)
        context.clickElementOnList(context.permission_available_list, run)
        time.sleep(2)
        context.clickOnElement(context.assign_button)

    def verify_run_permission_assign_to_group(context, run):
        time.sleep(3)
        context.getElementTextInList(context.permission_assigned_list, run)
        time.sleep(2)

    def unassign_run_permission_from_group(context, run):
        time.sleep(3)
        context.clickElementOnList(context.permission_assigned_list, run)
        time.sleep(2)
        context.clickOnElement(context.permission_unassign_button)

    def verify_un_assigned_run_permission_from_group(context, run):
        time.sleep(3)
        context.getElementTextInList(context.permission_available_list, run)
        time.sleep(2)

    def assigned_run_to_vehicle(context, run, vehicle):
        time.sleep(3)
        context.clickElementOnList(context.assignments_run_list, run)
        time.sleep(1)
        context.clickElementOnList(context.assignments_vehicle_list, vehicle)
        time.sleep(1)

    def click_on_assign_run_button(context):
        time.sleep(2)
        context.clickOnElement(context.assignment_add_button)

    def verify_run_assignment(context, run):
        time.sleep(3)
        context.getElementTextInList(context.assignment_assigned_runs_list, run)

    def remove_run_assignment(context, run):
        time.sleep(3)
        context.click_element_in_element(root_elm=context.root_element,
                                         sub_elm_one=context.element_one,
                                         sub_elm_two=context.element_two,
                                         text=run)

    def verify_assignment_remove(context, run):
        time.sleep(3)
        context.verifyVisibilityFalse(text=run, by_locator=context.assignment_assigned_runs_list)



