import logging
import time

from selenium.webdriver.common.by import By

from pages.base_page import BasePage

logging.basicConfig(filename='action.log', encoding='utf-8', level=logging.INFO)


class ServiceManagementPage(BasePage):
    # details elements
    vehicle_list = "//*[@id='vehiclesTree_grid'] //*[@class='leaf-primary-name']"
    select_type = (By.XPATH, "//span[contains(text(),'-- Select item --')]")
    normal_type = (By.XPATH, "//ul[@id='serviceType_listbox']/li[2]")
    minor_type = (By.XPATH, "//ul[@id='serviceType_listbox']/li[3]")
    major_type = (By.XPATH, "//ul[@id='serviceType_listbox']/li[1]")
    unschedule_type = (By.XPATH, "//ul[@id='serviceType_listbox']/li[4]")
    service_date = (By.ID, "serviceDate")
    service_cost = (By.XPATH, "serviceCost")
    update_button = (By.ID, "updateButton")
    delete_service_button = (By.XPATH, "(//*[@class='centerText']/button[1])[1]")
    save_grid_button = (By.ID, "updateGridButton")
    service_record = "//*[@id='serviceDetailsDataGrid'] //td[1]"
    date_field = (By.XPATH, "(//*[@id='serviceDetailsDataGrid'] //td[1])[1]")
    confirm_button = (By.ID, "ok_confirm1")
    start_odometer_field = (By.XPATH, "(//*[@id='normalPVServiceScheduleData'] //span[1]/input[1])[2]")

    def select_vehicle_from_vehicle_list(context, vehicle):
        time.sleep(2)
        context.clickElementOnList(by_locator=context.vehicle_list, vehicle_group=vehicle)

    def add_new_service_record(context, priority, cost, date):
        time.sleep(2)
        context.clickOnElement(context.select_type)
        if priority.lower() == 'normal':
            context.clickOnElement(context.normal_type)
        elif priority.lower() == 'minor':
            context.clickOnElement(context.minor_type)
        elif priority.lower() == 'major':
            context.clickOnElement(context.major_type)
        elif priority.lower() == 'unscheduled':
            context.clickOnElement(context.unschedule_type)
        else:
            logging.info("Type Not Specified")

        time.sleep(1)
        context.sendKeysToElement(context.service_cost, cost)
        context.sendKeysToElement(context.service_date, date)
        time.sleep(1)
        context.clickOnElement(context.update_button)
        time.sleep(3)

    def click_on_save_button(context):
        time.sleep(3)
        context.clickOnElement(context.save_grid_button)

    def verify_service_on_grid(context, date):
        time.sleep(3)
        context.getElementTextInList(context.service_record, date)

    def click_on_service_record(context, date):
        time.sleep(3)
        context.clickElementOnList(context.service_record, date)

    def edit_service_record_and_save(context,date_new):
        time.sleep(1)
        context.sendKeysToElement(context.date_field, date_new)
        time.sleep(1)
        context.clickOnElement(context.save_grid_button)
        time.sleep(1)

    def delete_the_service_record(context):
        time.sleep(2)
        context.clickOnElement(context.delete_service_button)
        time.sleep(1)
        context.clickOnElement(context.confirm_button)
        time.sleep(1)
        context.clickOnElement(context.save_grid_button)

    def add_start_odometer_and_save(context, odo):
        time.sleep(2)
        context.sendKeysToElement(context.start_odometer_field, odo)
        time.sleep(1)
        context.clickOnElement(context.update_button)

    def verify_changed_odometer_record(context, odo):
        time.sleep(1)
        context.verifyTextOnElement(context.start_odometer_field, text=odo)
